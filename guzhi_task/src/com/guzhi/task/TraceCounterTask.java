/**
 * 
 */
package com.guzhi.task;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.guzhi.common.Consts;
import com.guzhi.domain.LocateItem;
import com.guzhi.utils.JsonHelper;

/**
 * 统计附近老乡的人数
 * 
 * @author duanshao
 * 
 */
@Component
public class TraceCounterTask extends AbstractTask {

    // 所有的省份列表
    private final static List<Integer> LOCATE_PROVINCE_LIST = Arrays.asList(110000, 120000, 130000, 140000, 150000,
            210000, 220000, 230000, 310000, 320000, 330000, 340000, 350000, 360000, 370000, 410000, 420000, 430000,
            440000, 450000, 460000, 500000, 510000, 520000, 530000, 540000, 610000, 620000, 630000, 640000, 650000,
            710000, 810000, 820000);

    // 所有的省份城市信息，从citylist.txt读取
    private static List<LocateItem> provinceLocateItems;

    private static volatile boolean isInit = false;

    private void init() {
        if (isInit) {
            return;
        }
        try {
            List<String> lines = IOUtils.readLines(new InputStreamReader(this.getClass().getClassLoader()
                    .getResourceAsStream("citylist.txt"), "utf-8"));
            if (CollectionUtils.isNotEmpty(lines)) {
                provinceLocateItems = JsonHelper.fromJson(lines.get(0), new TypeReference<List<LocateItem>>() {
                });
                isInit = true;
            }
        } catch (IOException e) {
            log.error("[TraceCounter] can't read citylist.txt", e);
        }
    }

    public void execute() {
        init();
        log.info("[TraceCounter] task start...");

        if (CollectionUtils.isEmpty(provinceLocateItems)) {
            log.info("[TraceCounter] task abort, province citys is empty...");
            return;
        }
        for (Integer locateProv : LOCATE_PROVINCE_LIST) {
            for (LocateItem province : provinceLocateItems) {
                int provCode = NumberUtils.toInt(province.getId());
                int count = resource.countProvince(provCode, null, locateProv);
                String key = Consts.Cache.USER_LOCATE_PREFIX + provCode + "_" + locateProv;
                log.info("[TraceCounter/province] key:{},count:{}", key, count);
                // 保存省份的人数
                redis.setString(key, String.valueOf(count));

                // 统计省份的总人数
                count = resource.countUserProvince(provCode);
                key = Consts.Cache.USER_PROVINCE_PREFIX + provCode;
                log.info("[TraceCounter/province] province All key:{},count:{}", key, count);
                redis.setString(key, String.valueOf(count));

                List<LocateItem> cityItems = province.getCitys();
                if (CollectionUtils.isEmpty(cityItems)) {
                    continue;
                }
                for (LocateItem city : cityItems) {
                    int cityCode = NumberUtils.toInt(city.getId());
                    count = resource.countProvince(provCode, cityCode, locateProv);
                    key = Consts.Cache.USER_LOCATE_PREFIX + provCode + "_" + cityCode + "_" + locateProv;
                    log.info("[TraceCounter/city] key:{},count:{}", key, count);
                    // 保存城市的人数
                    redis.setString(key, String.valueOf(count));

                    // 统计市区的总人数
                    count = resource.countUserCity(cityCode);
                    key = Consts.Cache.USER_CITY_PREFIX + cityCode;
                    log.info("[TraceCounter/city] city All key:{},count:{}", key, count);
                    redis.setString(key, String.valueOf(count));
                }
            }
        }
    }
}
