/**
 * 
 */
package com.guzhi.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.guzhi.domain.DomainResource;
import com.guzhi.service.RedisService;
import com.guzhi.utils.ThreadPool;

/**
 * @author duanshao
 * 
 */
public abstract class AbstractTask {

    // 日志
    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected DomainResource resource;

    @Autowired
    protected ThreadPool threadPool;

    @Autowired
    protected RedisService redis;

}
