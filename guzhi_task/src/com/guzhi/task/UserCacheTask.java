/**
 * 
 */
package com.guzhi.task;

import org.springframework.stereotype.Component;

/**
 * @author duanshao
 * 
 */
@Component
public class UserCacheTask extends AbstractTask {
    public void execute() {
        log.info("[userCache] task start...");
    }
}
