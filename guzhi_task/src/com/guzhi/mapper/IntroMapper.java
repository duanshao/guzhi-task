package com.guzhi.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.guzhi.domain.Intro;

/**
 * @abthor Elson
 *         Nov 26, 2014 2:39:54 AM
 * @Email 372069412@qq.com
 */
public interface IntroMapper {

    @Select("select userId,content,url from ${table} where userId = #{userId} order by createAt desc limit #{offset} , #{size}")
    List<Intro> getIntros(@Param("table") String table, @Param("userId") Long userId, @Param("offset") Integer offset,
            @Param("size") Integer size);

    @Select("select count(1) from ${table} where userId = #{userId}")
    int getCount(@Param("table") String table, @Param("userId") Long userId);

    @Insert("insert into ${table}(userId,content,url,address,createAt)values(#{intro.userId},#{intro.content},#{intro.url},#{intro.address},current_timestamp)")
    int addIntro(@Param("table") String table, @Param("intro") Intro intro);

    @Select("select * from ${table} where userId = #{userId} order by createAt desc limit 1")
    Intro getIntro(@Param("table") String table, @Param("userId") Long userId);

    @Update("update ${table} set url = #{intro.url} where id = #{intro.id} and userId = #{intro.userId}")
    int upadteIntroUrl(@Param("table") String table, @Param("intro") Intro intro);

    @Select("select userId,content from ${table} where userId in (#{ids})")
    List<Intro> getIntrosById(@Param("table") String table, @Param("ids") String ids);

    @Update("update user_trace set intro = #{content} where userId = #{userId}")
    int updateIntroToUserTrace(@Param("userId") Long userId, @Param("content") String content);

}
