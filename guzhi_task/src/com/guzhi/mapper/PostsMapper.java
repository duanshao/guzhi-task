package com.guzhi.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.mapping.StatementType;

import com.guzhi.domain.Posts;
import com.guzhi.domain.PostsAlbum;
import com.guzhi.domain.PostsCategory;
import com.guzhi.domain.PostsQuery;
import com.guzhi.domain.Reply;
import com.guzhi.domain.ReplyResp;

/**
 * 帖子
 * 
 * @author duanshao
 * 
 */
public interface PostsMapper {

    /**
     * 创建帖子
     * 
     * @param post
     * @return
     */
    @SelectKey(before = true, statementType = StatementType.PREPARED, resultType = Long.class, statement = "select ifnull(max(id),${radix})+34 from ${table}", keyProperty = "posts.id")
    @Insert("insert into ${table}(id,userId,nick,logo,category,cateName,title,titlePinyin,content,"
            + "userIp,lat,lon,location,province,city,createAt) values("
            + "#{posts.id},#{posts.userId},#{posts.nick},#{posts.logo},"
            + "#{posts.category},#{posts.cateName},#{posts.title},#{posts.titlePinyin},#{posts.content},"
            + "#{posts.userIp},#{posts.lat},#{posts.lon},"
            + "#{posts.location},#{posts.province},#{posts.city},#{posts.createAt})")
    int create(@Param("table") String table, @Param("posts") Posts posts, @Param("radix") Integer radix);

    /**
     * 赞
     * 
     * @param postId
     * @return
     */
    @Update("update ${table} set likes = likes + 1 where id = #{id}")
    int like(@Param("table") String table, @Param("id") Long postId);

    /**
     * 更新最后回复信息
     * 
     * @param postId
     * @return
     */
    @Update("update ${table} set replys = replys+ 1 where id = #{id}")
    int reply(@Param("table") String table, @Param("id") Long postId);

    /**
     * 添加回复信息(有可能是点赞信息)
     * 
     * @param postId
     * @return
     */
    @SelectKey(before = true, statementType = StatementType.PREPARED, resultType = Long.class, statement = "select ifnull(max(id),${radix})+4 from ${table}", keyProperty = "reply.id")
    @Options(useGeneratedKeys = true, keyProperty = "reply.id")
    @Insert("insert into ${table}(id,postsId,postsUserId,types,userId,usernick,userLogo,"
            + "replyId,replyUserId,replyUsernick,content,userIp,masterReplyId,updateAt) values( #{reply.id},"
            + "#{reply.postsId},#{reply.postsUserId},#{reply.types},#{reply.userId},#{reply.usernick},#{reply.userLogo},#{reply.replyId},"
            + "#{reply.replyUserId},#{reply.replyUsernick},#{reply.content},#{reply.userIp},#{reply.masterReplyId},#{reply.updateAt})")
    int addReply(@Param("table") String table, @Param("reply") Reply reply, @Param("radix") Integer radix);

    /**
     * 获取帖子信息
     * 
     * @param postId 帖子id
     */
    @Select("select * from ${table} where id= #{postsId}")
    Posts getById(@Param("table") String table, @Param("postsId") Long postsId);

    /**
     * 
     * @return
     */
    @Select("select * from posts_category where current_timestamp between validAt and invalidAt order by id desc")
    List<PostsCategory> getAllCategory();

    /**
     * @param query
     * @return
     */
    List<Posts> query(PostsQuery query);

    /**
     * @param postsId
     * @return
     */
    @Select("select likes from ${table} where id= #{postsId}")
    int getLikes(@Param("table") String table, @Param("postsId") Long postsId);

    /**
     * @param postsId
     * @return
     */
    @Select("select replys from ${table} where id= #{postsId}")
    int getReplys(@Param("table") String table, @Param("postsId") Long postsId);

    /**
     * @param table
     * @param userId
     * @return
     */
    @Select("select * from ${table} where userId= #{userId} and id < ifnull(#{maxPostsId},100000000) order by id desc limit #{offset},#{size}")
    List<Posts> queryByUserId(@Param("table") String table, @Param("userId") Long userId,
            @Param("maxPostsId") Long maxPostsId, @Param("offset") int offset, @Param("size") int size);

    @Select("select * from ${table} where postsId in( ${postsIds})")
    List<PostsAlbum> getAlbum(@Param("table") String table, @Param("postsIds") String postsIds);

    /**
     * 去掉自己回复自己的或者自己点赞自己的护着自己评论自己的
     * 
     * @param table
     * @param userId
     * @param offset
     * @param size
     * @return
     */
    @Select("select * from (select * from ${table}_0 where postsUserId != userId and postsUserId = #{userId} and types != 1  union "
            + "select * from ${table}_0 where replyUserId != userId and replyUserId =#{userId} and types = 1 union "
            + "select * from ${table}_1 where postsUserId != userId and postsUserId = #{userId} and types != 1 union "
            + "select * from ${table}_1 where replyUserId != userId and replyUserId =#{userId} and types = 1 union "
            + "select * from ${table}_2 where postsUserId != userId and postsUserId = #{userId} and types != 1 union "
            + "select * from ${table}_2 where replyUserId != userId and replyUserId =#{userId} and types = 1 union "
            + "select * from ${table}_3 where postsUserId != userId and postsUserId = #{userId} and types != 1 union "
            + "select * from ${table}_3 where replyUserId != userId and replyUserId =#{userId} and types = 1  "
            + ") a order by updateAt desc limit #{offset},#{size} ")
    List<Reply> queryReplyByUserId(@Param("table") String table, @Param("userId") Long userId,
            @Param("offset") int offset, @Param("size") int size);

    /**
     * @param table
     * @param postsId
     */
    @Delete("delete from ${table} where id = #{postsId}")
    int delPosts(@Param("table") String table, @Param("postsId") Long postsId);

    /**
     * @param table
     * @param postsId
     */
    @Delete("delete from ${table} where postsId=#{postsId}")
    int delReplyByPostsId(@Param("table") String table, @Param("postsId") Long postsId);

    /**
     * @param table
     * @param postsId
     */
    @Delete("delete from ${table} where postsId=#{postsId}")
    int delAlbum(@Param("table") String table, @Param("postsId") Long postsId);

    /**
     * @param string
     * @param deduct
     */
    @Update("update ${table} set replys = replys - #{deduct} where id = #{postsId}")
    int deductReplys(@Param("table") String table, @Param("postsId") Long postsId, @Param("deduct") Integer deduct);

    /**
     * 删除某条评论的时候，连评论的回复也删除
     * 
     * @param table
     * @param postsId
     */
    @Delete("delete from ${table} where id=#{replyId} or replyId = #{replyId}")
    int delReplyById(@Param("table") String table, @Param("replyId") Long replyId);

    /**
     * @param userId
     * @param postsId
     * @return
     */
    @Select("select count(1) from ${table} where userId = #{userId} and postsId = #{postsId} and types =2 limit 1")
    int liked(@Param("table") String table, @Param("userId") Long userId, @Param("postsId") Long postsId);

    /**
     * @param id
     * @return
     */
    @Select("select * from posts_category where id = #{value}")
    PostsCategory getCategoryById(Long id);

    /**
     * @param table
     * @param postsId
     * @param i
     * @param pageSize
     * @return
     */
    @Select("select id ,userId,userLogo,usernick,content,updateAt from ${table} where postsId=#{postsId} and id > ifnull(#{minId},0) and types = 0 order by id limit #{offset},#{size}")
    List<ReplyResp> queryCommentsByPostsId(@Param("table") String table, @Param("postsId") Long postsId,
            @Param("minId") Long minId, @Param("offset") int offset, @Param("size") int size);

    /**
     * @param table
     * @param postsId
     * @param replyId
     * @param i
     * @param pageSize
     * @return
     */
    @Select("select id ,userId,userLogo,usernick,content,updateAt,replyId,replyUsernick from ${table} "
            + " where postsId=#{postsId} and masterReplyId=#{replyId} and types = 1 and id > ifnull(#{minId},0) order by id limit #{offset},#{size}")
    List<ReplyResp> queryCommentReply(@Param("table") String table, @Param("postsId") Long postsId,
            @Param("replyId") Long replyId, @Param("minId") Long minId, @Param("offset") int offset,
            @Param("size") int size);

    /**
     * @param table
     * @param postsId
     * @param replyId
     * @return
     */
    @Select("select count(1) from ${table} where postsId=#{postsId} and masterReplyId=#{replyId} "
            + "and types = 1 and id > ifnull(#{minId},0)")
    int queryCommentTotalReply(@Param("table") String table, @Param("postsId") Long postsId,
            @Param("replyId") Long replyId, @Param("minId") Long minId);

    /**
     * @param table
     * @param postsId
     * @param replyId
     * @return
     */
    List<Long> queryLikedPostsId(@Param("table") String table, @Param("userId") Long userId,
            @Param("postsIds0") String postsIds0, @Param("postsIds1") String postsIds1,
            @Param("postsIds2") String postsIds2, @Param("postsIds3") String postsIds3);

    /**
     * @param table
     * @param postsId
     * @param replyId
     * @return
     */
    @Select("select * from ${table} where id = #{replyId}")
    Reply queryReplyById(@Param("table") String table, @Param("replyId") Long replyId);

    /**
     * @param table
     * @param postsId
     * @param replyId
     * @return
     */
    @Select("select id ,userId,userLogo,usernick,content,updateAt,replyId,replyUsernick from ${table} where id = #{replyId}")
    ReplyResp queryReplyRespById(@Param("table") String table, @Param("replyId") Long replyId);

    /**
     * @param table
     * @param al
     * @return
     */
    @Insert("insert into ${table}(postsId,url,thumbnail)values(#{album.postsId},#{album.url},#{album.thumbnail})")
    int addAlbum(@Param("table") String table, @Param("album") PostsAlbum al);

    /**
     * @param table
     * @param postsId
     * @param userId
     * @return
     */
    @Select("select count(1) from ${table} where userId = #{userId} and types=2 and postsId =#{postsId}")
    int queryLikedByPostsIdAndUserID(@Param("table") String table, @Param("postsId") Long postsId,
            @Param("userId") Long userId);

}
