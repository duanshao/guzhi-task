package com.guzhi.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 
 * @author xiejunbo
 **/

public interface AdminMapper {

    /**
     * 获取某个省的用户数
     * @param provinceCode
     * @return
     */ 
    @Select("select counter from user_count where code = #{province}")
    int countProvince(@Param("province") Integer province);

    
}
