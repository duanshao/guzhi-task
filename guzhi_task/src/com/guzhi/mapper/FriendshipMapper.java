package com.guzhi.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.guzhi.domain.Friendship;

/**
 * author Elson
 * 2014年11月19日 下午3:01:07
 * email 372069412@qq.com
 */
public interface FriendshipMapper {

    /**
     * 添加好友
     * 
     * @param userId 用户ID
     * @param friendId 好友ID
     * 
     * */
    @Insert("replace into ${table}(userId,friendId)values(#{userId},#{friendId})")
    int addFriendship(@Param("table") String table, @Param("userId") Long userId, @Param("friendId") Long friendId);

    /**
     * 查询好友列表
     * 
     * @param userId
     * @return List<Friendship>
     * */
    @Select("select * from ${table} where userId = #{userId} and state = 0")
    List<Friendship> query(@Param("table") String table, @Param("userId") Long userId);

    /**
     * 查询黑名单列表
     * 
     * @param userId
     * */
    @Select("select friendId from ${table} where userId = #{userId} and state = 1")
    List<Long> queryMyBlack(@Param("table") String table, @Param("userId") Long userId);

    /**
     * 查询我被哪些人拉黑的列表
     * 
     * @param userId
     * */
    @Select("select userId from friendship_0 where friendId = #{value} and state = 1 union "
            + " select userId from friendship_1 where friendId = #{value} and state = 1 union "
            + " select userId from friendship_2 where friendId = #{value} and state = 1 union "
            + " select userId from friendship_3 where friendId = #{value} and state = 1 union "
            + " select userId from friendship_4 where friendId = #{value} and state = 1 union "
            + " select userId from friendship_5 where friendId = #{value} and state = 1 union "
            + " select userId from friendship_6 where friendId = #{value} and state = 1 union "
            + " select userId from friendship_7 where friendId = #{value} and state = 1 ")
    List<Long> queryToBeBlacker(Long friendId);

    /**
     * 删除好友
     * 
     * @param userId 用户id
     * @param friendId 好友id
     * @return resp
     * */
    @Delete("delete from ${table} where userId = #{userId} and friendId = #{friendId}")
    int delete(@Param("table") String table, @Param("userId") Long userId, @Param("friendId") Long friendId);

    /**
     * @param userId
     * @param blackUserId
     * @return
     */
    @Insert("replace into ${table}(userId,friendId,state)values(#{userId},#{blackUserId},1)")
    int addBlack(@Param("table") String table, @Param("userId") Long userId, @Param("blackUserId") Long blackUserId);

}
