package com.guzhi.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.guzhi.domain.LeadPage;

/**
 * 引导页
 * @author xiejunbo
 * @email 624664181@qq.com
 * @version 1.0.0
 * @date 2014年11月20日 上午11:33:46
 **/

public interface LeadPageMapper {

    /**
     * 保存引导页面图片地址
     * @param url
     * @return
     */
    @Insert("insert into leadpage(url)values(#{url})")
    int saveLeadPageUrl(@Param("url") String url);

    
    /**
     * 查询引导页面图片地址
     * @return
     */
    @Select("SELECT url FROM leadpage WHERE id = (SELECT MAX(id) FROM leadpage);")
    String query();

    
}
