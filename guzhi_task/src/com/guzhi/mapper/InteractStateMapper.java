package com.guzhi.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.guzhi.domain.InteractState;

/**
 * @abthor Elson
 *         Dec 7, 2014 4:21:07 PM
 * @Email 372069412@qq.com
 */

public interface InteractStateMapper {

    @Insert("replace into interact_state(userId,friendId,state,createAt)values(#{userId},#{friendId},1,current_timestamp)")
    int addUserIdInteract(@Param("userId") Long userId, @Param("friendId") Long friendId);

    @Insert("replace into interact_state(userId,friendId,state,createAt)values(#{toUserId},#{fromUserId},2,current_timestamp)")
    int addFriendIdInteract(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Update("update interact_state set state = 3  where "
            + "(userId = #{fromUserId} and friendId = #{toUserId}) or (userId = #{toUserId} and friendId = #{fromUserId})")
    int agreeInteract(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Update("update interact_state set state = 7  where userId = #{fromUserId} and friendId = #{toUserId}")
    int rejectInteract(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Update("update interact_state set state = 8  where userId = #{toUserId} and friendId = #{fromUserId}")
    int beReject(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Update("update interact_state set state = 2 where userId = #{fromUserId} and friendId = #{toUserId}")
    int recoverInteract(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Update("update interact_state set state = 1 where userId = #{toUserId} and friendId = #{fromUserId}")
    int beRecover(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Select("select * from interact_state where userId = #{fromUserId} and friendId = #{toUserId}")
    InteractState getInteract(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    /**
     * 陌生人不需要返回
     * 
     * @param userId
     * @return
     */
    @Select("select * from interact_state where userId = #{value} and state <> 0")
    List<InteractState> getByUserId(Long userId);

    @Update("update interact_state set state = 6  where "
            + "(userId = #{fromUserId} and friendId = #{toUserId}) or (userId = #{toUserId} and friendId = #{fromUserId})")
    int blackEachOther(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Update("update interact_state set state = 4 where userId = #{fromUserId} and friendId = #{toUserId}")
    int black(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Update("update interact_state set state = 5 where userId = #{toUserId} and friendId = #{fromUserId}")
    int beBlack(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Update("update interact_state set state = 0 where userId = #{fromUserId} and friendId = #{toUserId}")
    int delete(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Update("update interact_state set state = 9 where userId = #{toUserId} and friendId = #{fromUserId}")
    int beDelete(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Update("update interact_state set state = 5 where userId = #{toUserId} and friendId = #{fromUserId}")
    int unblack(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Update("update interact_state set state = 4 where userId = #{toUserId} and friendId = #{fromUserId}")
    int beUnBlack(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    @Select("select a.userId from interact_state a, (select d.friendId from interact_state d where d.userId = #{userId}) c where a.friendId = c.friendId and a.userId <> #{userId} and a.friendId <> #{userId} group by a.userId having count(1) >= 2 limit 10")
    List<Long> getRecommend(@Param("userId") Long userId);

}
