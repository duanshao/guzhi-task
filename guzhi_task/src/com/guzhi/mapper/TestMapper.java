package com.guzhi.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.guzhi.domain.Test;

public interface TestMapper {
	
    @Select("select * from `test`")
    List<Test> queryAll();

    @Select("select * from `test` where id = #{id}")
    Test queryTestById(@Param("id") Long id);
    
}
