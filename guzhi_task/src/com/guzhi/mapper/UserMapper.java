package com.guzhi.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.guzhi.domain.User;

public interface UserMapper {

    /**
     * 查询咕吱号是否存在
     * 
     * @param gzno 咕吱号
     * @return 统计数
     * */
    @Select("select count(1) from ${table} where gzno = #{gzno}")
    int hasGzno(@Param("table") String table, @Param("gzno") String gzno);

    /**
     * 
     * @param table
     * @param phone
     * @param pwd
     * @param gzno
     * @param radix 可以防止表中没有数据时，可以使用出事ID
     * @param nick
     * @param
     * @return
     */
    @Options(useGeneratedKeys = true, keyProperty = "user.id")
    @Insert("insert into ${table}(id,phone,pwd,constell,age,gzno,nick,province,city,district,sex,birth,imid,width,height,hdlogo,logo,createAt)"
            + " select ifnull(max(id),${radix})+8 ,"
            + "#{user.phone},#{user.pwd},#{user.constell},#{user.age},#{gzno},#{user.nick},#{user.province},#{user.city},"
            + "#{user.district},#{user.sex},#{user.birth},md5(#{user.phone}),#{user.width},#{user.height},#{user.hdLogo},#{user.logo},current_timestamp from ${table}")
    int create(@Param("table") String table, @Param("user") User user, @Param("gzno") String gzno,
            @Param("radix") Integer radix);

    /**
     * 用户登录
     * 
     * @param userName 帐号
     * @param pwd 密码
     * 
     * */
    @Select("select * from ${table} where (gzno = #{userName} or phone = #{userName}) and pwd = #{pwd}")
    User login(@Param("table") String table, @Param("userName") String userName, @Param("pwd") String pwd);

    /**
     * 更新用户的imid
     * 
     * @param table
     * @param phone
     * @param imid
     * @return
     */
    @Update("update ${table} set imid = md5(#{phone}) where phone = #{phone}")
    int updateImid(@Param("table") String table, @Param("phone") String phone);

    /**
     * 根据phone查询用户
     * 
     * */
    @Select("select id,gzno,province,city,nick,birth,hdLogo,logo,phone,age,constell,width,height,"
            + " intro, school,company,hobby,job,district,sex,visible,imid from ${table} where phone = #{phone}")
    User getUser(@Param("table") String table, @Param("phone") String phone);

    /**
     * 根据phone或gzno查询用户
     * 
     * */
    @Select("select id,gzno,province,city,nick,birth,hdLogo,logo,phone,age,constell,"
            + "intro, school,company,hobby,job,district,sex,visible,imid,width,height from "
            + "${table} where (gzno = #{username} or phone = #{username}) and visible = 1")
    User getUserByUserName(@Param("table") String table, @Param("username") String username);

    /**
     * 根据userId查询用户
     * */
    @Select("select id,gzno,province,city,nick,birth,hdLogo,logo,phone,age,constell,"
            + "school,company,hobby,job,district,sex,visible,imid,width,height from ${table} where id = #{id}")
    User getUserById(@Param("table") String table, @Param("id") Long id);

    /**
     * 根据phone查询用户
     * */
    @Select("select id,gzno,province,city,nick,birth,hdLogo,logo,phone,age,constell,"
            + "school,company,hobby,job,district,sex,visible,imid,width,height from ${table} where phone = #{phone}")
    User getUserByPhone(@Param("table") String table, @Param("phone") Long phone);

    /**
     * 根据userId更新用户信息
     * */
    int updateInfo(@Param("table") String table, @Param("userId") Long userId, @Param("user") User user);

    /**
     * @param table
     * @param field
     * @param value
     * @return
     */
    @Update("update ${table} set ${field}=#{val} where id = #{userId}")
    int updateByField(@Param("table") String table, @Param("field") String field, @Param("val") Object value,
            @Param("userId") Long userId);

    /**
     * 检查点赞记录
     * 
     * @param fromUserId
     * @param toUserId
     * @return
     */
    @Select("select count(1) from user_like where fromUserId = #{fromUserId} and toUserId = #{toUserId}")
    int existLike(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId);

    /**
     * 保存点赞记录
     * 
     * @param fromUserId
     * @param toUserId
     * @param fromUserImid
     * @param toUserImid
     * @return
     */
    @Insert("insert into user_like(fromUserId,toUserId,fromUserImid,toUserImid)"
            + "values(#{fromUserId},#{toUserId},#{fromUserImid},#{toUserImid})")
    int addLike(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId,
            @Param("fromUserImid") String fromUserImid, @Param("toUserImid") String toUserImid);

    /**
     * likes字段自增1
     * 
     * @param toUserId
     * @return
     */
    @Update("update user_trace set likes = #{likes} where userId = #{toUserId}")
    int updateLike(@Param("toUserId") Long toUserId, @Param("likes") int likes);

    /**
     * 获取用户点赞数
     * 
     * @param toUserId
     * @return
     */
    @Select("select likes from user_trace where userId = #{toUserId}")
    Integer getLikes(@Param("toUserId") Long toUserId);

    /**
     * 获取点赞者信息
     * 
     * @param fromUserId
     * @return
     */
    @Select("select nick, logo from ${tableName} where id=#{fromUserId}")
    User getLikerInfo(@Param("tableName") String tableName, @Param("fromUserId") Long fromUserId);

    /**
     * 获取logo
     * 
     * @param tableName
     * @param fromUserId
     * @return
     */
    @Select("select logo from ${tableName} where id=#{fromUserId}")
    String getLogo(@Param("tableName") String tableName, @Param("fromUserId") Long fromUserId);

    /**
     * 获取昵称
     * 
     * @param tableName
     * @param fromUserId
     * @return
     */
    @Select("select nick from ${tableName} where id=#{fromUserId}")
    String getNick(@Param("tableName") String tableName, @Param("fromUserId") Long fromUserId);

    /**
     * 获取userId
     * 
     * @param phone
     * @return
     */
    @Select("select id from ${tableName} where phone=#{phoneNumber}")
    int getUserId(@Param("tableName") String tableName, @Param("phoneNumber") Long phoneNumber);

    /**
     * @param table
     * @param ids
     * @return
     */
    @Select("select id,gzno,province,city,district,birth,nick,age,hdLogo,logo,constell,intro,school,company,hobby,job,sex,width,height,imid from ${table} where id in (${ids})")
    List<User> getUserByIds(@Param("table") String table, @Param("ids") String ids);

    /**
     * 通过phone获取imid
     * 
     * @param phone　
     * @return　
     */
    @Select("select imid from ${tableName} where phone = #{phone}")
    String getImidByPhone(@Param("tableName") String tableName, @Param("phone") String phone);

    /**
     * 通过phone获取密码
     * 
     * @param tableName
     * @param phone
     * @return
     */
    
    @Select("select pwd from ${tableName} where phone = #{phone}")
    String getOldPassword(@Param("tableName") String tableName, @Param("phone") String phone);

    @Insert("replace into imid_userId values(#{imid},#{userId})")
    int createImidUserId(@Param("imid") String imid, @Param("userId") Long userId);

    @Select("select userId from imid_userId where imid in(${imids})")
    List<Long> getUserIdByImids(@Param("imids") String imids);

    /**
     * 统计省市用户数
     * 
     * @param provinceCode
     * @param cityCode
     * @return
     */
    @Update("update user_count set counter=counter+1 where code=#{provinceCode} or code = #{cityCode}")
    int updateUserCount(@Param("provinceCode") Integer provinceCode, @Param("cityCode") Integer cityCode);

    /**
     * 
     * @param tableName   表名
     * @param username    手机号或咕吱号
     * @return
     */
    @Select("select id from ${tableName} where phone=#{username} or gzno=#{username}")
    Long getUserIdByUserName(@Param("tableName") String tableName, @Param("username") String username);

    /**
     * 批量注册用户
     * @param gzno
     * @param pwd
     * @param province
     * @param city
     * @param nick
     * @param birth
     * @param hdLogo
     * @param logo
     * @param phone
     * @param age
     * @param sex
     * @param imid
     * @return
     */
    @Options(useGeneratedKeys = true, keyProperty = "user.id")
    @Insert("insert into ${table}(id,phone,pwd,constell,age,gzno,nick,province,city,district,intro,school,sex,birth,imid,width,height,hdlogo,logo,createAt)"
            + " select ifnull(max(id),${radix})+8 ,"
            + "#{user.phone},#{user.pwd},#{user.constell},#{user.age},#{user.gzno},#{user.nick},#{user.province},#{user.city},"
            + "#{user.district},#{user.intro},#{user.school},#{user.sex},#{user.birth},#{user.imid},#{user.width},#{user.height},#{user.hdLogo},#{user.logo},current_timestamp from ${table}")
    int genUser( @Param("user")User user, @Param("table")String table,@Param("radix") int radix);

    /**
     * 新增用户轨迹记录
     * @param toUserId
     * @return
     */
    @Insert("insert into user_trace(gzno,userId,likes,sex) values(#{gzno},#{toUserId},1,#{sex})")
    int initLike(@Param("gzno")String gzno,@Param("toUserId")Long toUserId,@Param("sex") int sex);

    /**
     * 获取性别
     * @param table
     * @param toUserId
     * @return
     */
    @Select("select sex from ${table} where id = #{toUserId}")
    int getSexByUserId(@Param("table")String table, @Param("toUserId")Long toUserId);

    /**
     * 获取咕吱号
     * @param table
     * @param toUserId
     * @return
     */
    @Select("select gzno from ${table} where id = #{toUserId}")
    String getGznoById(@Param("table") String table, @Param("toUserId") Long toUserId);

}
