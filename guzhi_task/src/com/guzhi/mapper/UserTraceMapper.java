package com.guzhi.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.guzhi.domain.UserTrace;
import com.guzhi.domain.UserTraceQuery;

/**
 * 用户地理位置表
 * 
 * @author duanshao
 * 
 */
public interface UserTraceMapper {

    /**
     * 保存轨迹信息
     * 
     * @param post
     * @return
     */
    @Insert("replace into user_trace(userId,gzno,lat,lon,sex,likes,visible,city,province)"
            + " values(#{userId},#{gzno},#{lat},#{lon},#{sex},#{likes},1,#{city},#{province})")
    int save(UserTrace trace);

    double ER = 6366.564864;// earth’s radius //地球半径

    /**
     * 按性别和区县查找出某个位置附近dist公里的用户
     * 
     * @param lat 纬度
     * @param lon 经度
     * @param dist dist公里范围
     * @param latLeft
     * @param latRight
     * @param lonLeft
     * @param lonRight
     * @param sex
     * @param offset
     * @param limit
     * @return
     */
    List<UserTrace> query(UserTraceQuery query);

    @Update("update user_trace set visible = 0 where userId=#{value}")
    int hide(Long userId);

    @Update("update user_trace set likes = likes+1 where userId=#{value}")
    int like(Long userId);

    /**
     * 批量添加用户时添加用户轨迹
     * 
     * @param gzno
     * @param userId
     * @param lat
     * @param lon
     * @param sex
     * @param city
     * @param province
     * @param intro
     * @return
     */
    @Insert("replace into user_trace(gzno,userId,lat,lon,sex,city,province,intro)values"
            + "(#{gzno},#{userId},#{lat},#{lon},#{sex},#{city},#{province},#{intro})")
    int addTrace(@Param("gzno") String gzno, @Param("userId") int userId, @Param("lat") double lat,
            @Param("lon") double lon, @Param("sex") int sex, @Param("city") int city, @Param("province") int province,
            @Param("intro") String intro);

    int countProvince(@Param("province") Integer province, @Param("city") Integer city,
            @Param("locateProv") Integer locateProv);

    /**
     * @param province
     * @return
     */
    @Select("select count(1) from user_trace where province = #{value}")
    int countUserProvince(Integer province);

    /**
     * @param city
     * @return
     */
    @Select("select count(1) from user_trace where city = #{value}")
    int countUserCity(Integer city);

}
