package com.guzhi.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.guzhi.domain.Interact;

/**
 * 互动表
 * 
 * @author duanshao
 * 
 */
public interface InteractMapper {

    @Insert("insert into interact(fromUserId,toUserId,`type`,content,createAt)values(#{fromUserId},#{toUserId},#{type},#{content},current_timestamp)")
    int add(Interact interact);

    @Select("select count(1) from interact where fromUserId=#{fromUserId} and toUserId=#{toUserId} and `type`=#{type}")
    int has(@Param("fromUserId") Long fromUserId, @Param("toUserId") Long toUserId, @Param("type") Integer type);

    @Select("select * from interact where toUserId = #{userId} order by createAt desc limit #{offset},#{size}")
    List<Interact> query(@Param("userId") Long userId, @Param("offset") Integer offset, @Param("size") Integer size);
}
