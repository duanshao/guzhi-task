package com.guzhi.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import com.guzhi.domain.Interact;

/**
 * 保存用户反馈信息
 * @author xiejunbo
 * @email 624664181@qq.com
 * @version 1.0.0
 * @date 2014年11月18日 下午4:47:59
 **/

public interface FeedBackMapper {

     
    /**
     * 保存反馈信息
     * @param gzno
     * @param content
     * @return
     */
    @Insert("insert into feedback(userId,content)values(#{userId},#{content})")
    int saveFeedBack(@Param("userId") Long userId, @Param("content") String content);
    
    
}
