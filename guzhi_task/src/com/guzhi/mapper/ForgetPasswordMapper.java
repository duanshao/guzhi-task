package com.guzhi.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

/**
 * 忘记密码
 * 
 * @author xiejunbo
 * @email 624664181@qq.com
 * @version 1.0.0
 * @date 2014年11月24日 下午4:27:06
 **/

public interface ForgetPasswordMapper {

    /**
     * 验证手机号是否存在
     * 
     * @param phone 手机号
     * @param table
     * @return
     */
    @Select("select count(*) from ${table} where phone = #{phone}")
    Integer existPhone(@Param("table") String table, @Param("phone") String phone);

    /**
     * 更新密码
     * 
     * @param password 密码
     * @param userId 
     * @param table　表名
     * @return
     */
    @Update("update ${tableName} set pwd = #{password} where phone = #{phone}")
    int updatePassword(@Param("password") String password, @Param("phone") String phone, @Param("tableName") String tableName);

}
