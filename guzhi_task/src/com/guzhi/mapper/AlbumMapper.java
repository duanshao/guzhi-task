package com.guzhi.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.guzhi.domain.Album;

public interface AlbumMapper {

    /**
     * 返回特定用户的图片列表
     * 
     * @param gzno
     * @return
     */
    @Select("select url,thumbnails from ${table} where userId = #{userId} order by createAt desc")
    List<Album> query(@Param("table") String table, @Param("userId") Long userId);
                                                                                    
    @Insert("insert into ${table}(userId,url,thumbnails,crc,createAt) values(#{a.userId},#{a.url},#{a.thumbnails},md5(#{a.url}),current_timestamp)")
    int add(@Param("table") String table, @Param("a") Album album);
    
    @Delete("delete from ${table} where userId=#{userId} and crc=#{crc}")
    int delete(@Param("table") String table, @Param("userId") Long userId, @Param("crc") String crc);


    @Update("update ${table} set url = #{album.url},thumbnails = #{album.thumbnails} where userId = #{album.userId} and crc = #{album.crc}")
	int update(@Param("table") String table,@Param("album") Album album);
    
}
