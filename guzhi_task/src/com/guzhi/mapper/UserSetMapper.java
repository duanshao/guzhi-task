package com.guzhi.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface UserSetMapper {

	@Insert("insert into user_background(code,url)values(#{code},#{fileName})")
	int addUserBackground(@Param("code") Integer code,@Param("fileName") String fileName);

	@Select("select url from user_background where code = #{code}")
	String getUserBackground(@Param("code") Integer code);

}
