package com.guzhi.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

public interface UserSet {

	@Insert("insert into user_background(code,url)values(#{code},#{fileName})")
	int addUserBackground(@Param("code") Integer code,@Param("fileName") String fileName);

}
