package com.guzhi.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.guzhi.domain.Discussion;

/**
 * 讨论组mapper
 * 
 * @author duanshao
 * 
 */
public interface DiscussionMapper {

    /**
     * 创建讨论组
     * 
     * @param dis
     * @return
     */
    @Insert("insert into discussion(leader,logo,name)values(#{leader},#{logo},#{name})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int createDiscussion(Discussion dis);

    /**
     * 删除讨论组
     * 
     * @param dis
     * @return
     */
    @Insert("delete from discussion where id = #{value}")
    int deleteDiscussion(Long discussionId);

    /**
     * 删除所有讨论组成员
     * 
     * @param dis
     * @return
     */
    @Insert("delete from ${table} where discussionId = #{discussionId}")
    int deleteAllMember(@Param("table") String table, @Param("discussionId") Long discussionId);

    /**
     * 删除某个讨论组成员
     * 
     * @param dis
     * @return
     */
    @Insert("delete from ${table} where discussionId = #{discussionId} and memberId=#{memberId}")
    int deleteMember(@Param("table") String table, @Param("discussionId") Long discussionId,
            @Param("memberId") Long memberId);

    /**
     * 批量添加组成员
     * 
     * @param table
     * @param sql
     * @return
     */
    @Insert("insert into ${table}(discussionId,memberId) values ${sql}")
    int batchAddDiscussionMember(@Param("table") String table, @Param("sql") String sql);

    /**
     * 批量添加组成员
     * 
     * @param table
     * @param sql
     * @return
     */
    @Insert("insert into ${table} (discussionId,memberId) values (#{discussionId},#{memberId})")
    int addDiscussionMember(@Param("table") String table, @Param("discussionId") Long discussionId,
            @Param("memberId") Long memberId);

    /**
     * 返回讨论组成员ID
     * 
     * @param gzno
     * @return
     */
    @Select("select memberId from ${table} where discussionId = #{discussionId}")
    List<Long> queryDiscussionMembers(@Param("table") String table, @Param("discussionId") Long discussionId);

    /**
     * 返回指定的会员的讨论组信息
     * 
     * @param memberId
     * @return
     */
    @Insert("select * from discussion_member_0 where memberId = #{memberId} union "
            + " select * from discussion_member_1 where memberId = #{memberId} union "
            + " select * from discussion_member_2 where memberId = #{memberId} union "
            + " select * from discussion_member_3 where memberId = #{memberId} union "
            + " select * from discussion_member_4 where memberId = #{memberId} union "
            + " select * from discussion_member_5 where memberId = #{memberId} union "
            + " select * from discussion_member_6 where memberId = #{memberId} union"
            + " select * from discussion_member_7 where memberId = #{memberId} ")
    List<Discussion> queryDiscussions(@Param("memberId") Long memberId);

}
