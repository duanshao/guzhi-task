package com.guzhi.domain;

import java.util.Date;

/**
 * 引导页LeadPage bean
 * @author xiejunbo
 * @email 624664181@qq.com
 * @version 1.0.0
 * @date 2014年11月20日 上午11:26:15
 **/

public class LeadPage {
    
    private Integer id;
    
    private String url;
    
    private Date uploadTime;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the uploadTime
     */
    public Date getUploadTime() {
        return uploadTime;
    }

    /**
     * @param uploadTime the uploadTime to set
     */
    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }
    
}
