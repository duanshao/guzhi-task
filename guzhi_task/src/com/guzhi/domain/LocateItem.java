package com.guzhi.domain;

import java.util.List;

public class LocateItem {
    private List<LocateItem> citys;
    private String id;
    private String name;

    public List<LocateItem> getCitys() {
        return citys;
    }

    public void setCitys(List<LocateItem> citys) {
        this.citys = citys;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}