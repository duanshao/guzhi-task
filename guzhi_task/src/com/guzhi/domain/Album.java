package com.guzhi.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author duanshao
 * 
 */
@JsonInclude(Include.NON_NULL)
public class Album {

    private Long id;

    private Long userId;

    private String gzno;

    private String url;

    private String thumbnails;

    private Date createAt;

    private String crc;// url的MD5值，后期可以通过gzno和crc值来删除指定的图片

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getGzno() {
        return gzno;
    }

    public void setGzno(String gzno) {
        this.gzno = gzno;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getCrc() {
        return crc;
    }

    public void setCrc(String crc) {
        this.crc = crc;
    }

    public String getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(String thumbnails) {
        this.thumbnails = thumbnails;
    }

	@Override
	public String toString() {
		return "Album [id=" + id + ", userId=" + userId + ", gzno=" + gzno
				+ ", url=" + url + ", thumbnails=" + thumbnails + ", createAt="
				+ createAt + ", crc=" + crc + "]";
	}
    
    

}
