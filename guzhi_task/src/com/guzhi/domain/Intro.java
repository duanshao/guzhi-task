package com.guzhi.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Intro {

	private Long id;
	//用户ID
	private Long userId;
	//咕吱签名内容
	private String content;
	//咕吱签名图片
	private String url;
	//发布的地址
	private String address;
	//发布时间
	private Date createAt;
	
	//统计签名数
	private Integer count;
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	
	@Override
	public String toString() {
		return "Intro [id=" + id + ", userId=" + userId + ", content="
				+ content + ", url=" + url + ", address=" + address
				+ ", createAt=" + createAt + "]";
	}

}
