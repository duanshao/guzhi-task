package com.guzhi.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 
 * @author xiejunbo
 * @email 624664181@qq.com
 * @version 1.0.0
 * @date 2014年12月19日 下午2:41:34
 **/

@JsonInclude(Include.NON_NULL)
public class PostsCategory {

    private Long id;

    private String name;// 类型名

    private String content;// 类型信息描述

    private String url;// 图片地址

    private Date validAt;// 有效开始时间

    private Date invalidAt;// 无效时间

    private Date updateAt;

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    public Date getValidAt() {
        return validAt;
    }

    public void setValidAt(Date validAt) {
        this.validAt = validAt;
    }

    public Date getInvalidAt() {
        return invalidAt;
    }

    public void setInvalidAt(Date invalidAt) {
        this.invalidAt = invalidAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public String toString() {
        return "PostsCategory [id=" + id + ", name=" + name + ", content=" + content + ", validAt=" + validAt
                + ", invalidAt=" + invalidAt + ", updateAt=" + updateAt + "]";
    }

}
