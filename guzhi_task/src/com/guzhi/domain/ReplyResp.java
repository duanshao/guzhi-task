/**
 * 
 */
package com.guzhi.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author duanshao
 * 
 */
@JsonInclude(Include.NON_NULL)
public class ReplyResp {
    private Long id;
    private Long userId;
    private String userLogo;
    private String usernick;
    private String content;
    private Date updateAt;
    private Long replyId;
    private String replyUsernick;
    private List<ReplyResp> replys; // 对应的回复
    private int remainReplys;// 当本记录是一条评论时，这个字段保存是对应的还有多少条回复

    public Long getReplyId() {
        return replyId;
    }

    public void setReplyId(Long replyId) {
        this.replyId = replyId;
    }

    public String getReplyUsernick() {
        return replyUsernick;
    }

    public void setReplyUsernick(String replyUsernick) {
        this.replyUsernick = replyUsernick;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<ReplyResp> getReplys() {
        return replys;
    }

    public void setReplys(List<ReplyResp> replys) {
        this.replys = replys;
    }

    public int getRemainReplys() {
        return remainReplys;
    }

    public void setRemainReplys(int remainReplys) {
        this.remainReplys = remainReplys;
    }

    public String getUserLogo() {
        return userLogo;
    }

    public void setUserLogo(String userLogo) {
        this.userLogo = userLogo;
    }

    public String getUsernick() {
        return usernick;
    }

    public void setUsernick(String usernick) {
        this.usernick = usernick;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

}
