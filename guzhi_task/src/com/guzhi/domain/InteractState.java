package com.guzhi.domain;

import java.util.Date;

public class InteractState {

    // 当前用户ID
    private Long userId;
    // 对方用户ID
    private Long friendId;
    // 当前用户状态
    private Integer state;
    // 对方用户状态
    private Date createAt;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFriendId() {
        return friendId;
    }

    public void setFriendId(Long friendId) {
        this.friendId = friendId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
}
