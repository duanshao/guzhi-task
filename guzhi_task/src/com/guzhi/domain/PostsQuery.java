/**
 * 
 */
package com.guzhi.domain;

/**
 * posts查询DTO
 * 
 * @author duanshao
 * 
 */
public class PostsQuery {
    private Double lon;
    private Double lat;
    private Integer category;
    private String searchKey;
    private Integer province;
    private Integer offset;
    private Integer size;
    private String table;
    private Double dist;
    private Double latLeft;
    private Double latRight;
    private Double lonLeft;
    private Double lonRight;
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Double getDist() {
        return dist;
    }

    public void setDist(Double dist) {
        this.dist = dist;
    }

    public Double getLatLeft() {
        return latLeft;
    }

    public void setLatLeft(Double latLeft) {
        this.latLeft = latLeft;
    }

    public Double getLatRight() {
        return latRight;
    }

    public void setLatRight(Double latRight) {
        this.latRight = latRight;
    }

    public Double getLonLeft() {
        return lonLeft;
    }

    public void setLonLeft(Double lonLeft) {
        this.lonLeft = lonLeft;
    }

    public Double getLonRight() {
        return lonRight;
    }

    public void setLonRight(Double lonRight) {
        this.lonRight = lonRight;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public Integer getProvince() {
        return province;
    }

    public void setProvince(Integer province) {
        this.province = province;
    }
}
