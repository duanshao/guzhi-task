package com.guzhi.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 用户位置表
 * 
 * @author duanshao
 * 
 */
@JsonInclude(Include.NON_NULL)
public class UserTrace {
    /**
     * 咕吱号
     */
    private String gzno;
    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户点赞数
     */
    private Integer likes;

    /**
     * 纬度
     */
    private Double lat;

    /**
     * 经度
     */
    private Double lon;

    /**
     * 性别
     */
    private Integer sex;
    /**
     * 老家城市
     */
    private Integer city;
    /**
     * 老家区、省
     */
    private Integer province;

    /**
     * 最后更新时间
     */
    private Date updateAt;

    /**
     * 是否可见
     */
    private Integer visible;
    /**
     * 用户资料
     * */
    private User user;

    /**
     * 咕吱签名
     * */
    private String intro;
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    
    /**
     * 真实的距离
     */
    private Double dist;

    public Double getDist() {
        return dist;
    }

    public void setDist(Double dist) {
        if (dist == null) {
            return;
        }
        BigDecimal bd = new BigDecimal(dist);
        this.dist = bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getProvince() {
		return province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getGzno() {
        return gzno;
    }

    public void setGzno(String gzno) {
        this.gzno = gzno;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }
    
    public String getIntro() {
		return intro;
	}

	public void setIntro(String intro) {
		this.intro = intro;
	}

	
	@Override
    public String toString() {
        return "UserTrace [gzno=" + gzno + ", userId=" + userId + ", likes=" + likes + ", lat=" + lat + ", lon=" + lon
                + ", sex=" + sex + ", updateAt=" + updateAt + "]";
    }

}
