package com.guzhi.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * author Elson
 * 2014年11月19日 下午4:55:11
 * email 372069412@qq.com
 */
@JsonInclude(Include.NON_NULL)
public class Friendship {

    // 主键id
    private Long id;
    // 用户id
    private Long userId;
    // 好友id
    private Long friendId;
    // 好友昵称
    private String nick;
    // 好友备注
    private String remark;
    // 分组id
    private Short groupId;
    // 好友状态 0为正常好友 , 1则黑名单
    private Short state;
    // 好友信息
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFriendId() {
        return friendId;
    }

    public void setFriendId(Long friendId) {
        this.friendId = friendId;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Short getGroupId() {
        return groupId;
    }

    public void setGroupId(Short groupId) {
        this.groupId = groupId;
    }

    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
