package com.guzhi.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TraceUserCount {

	private Integer provinceCount;

	private Integer cityCount;
	
	@JsonInclude(Include.ALWAYS)
	private List data;
	
	private int page;
	
	private int pageCount;

	public Integer getProvinceCount() {
		return provinceCount;
	}

	public void setProvinceCount(Integer provinceCount) {
		this.provinceCount = provinceCount;
	}

	public Integer getCityCount() {
		return cityCount;
	}

	public void setCityCount(Integer cityCount) {
		this.cityCount = cityCount;
	}

	public List getData() {
		return data;
	}

	public void setData(List data) {
		this.data = data;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		
		this.pageCount = pageCount;
	}
	
}
