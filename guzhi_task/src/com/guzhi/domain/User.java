package com.guzhi.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 用户bean
 * author Elson
 * 2014年11月5日
 * email 372069412@qq.com
 */
@JsonInclude(Include.NON_NULL)
public class User {

    // 自增ID
    private Long id;
    // 咕吱号码
    private String gzno;
    // 用户密码
    private String pwd;
    // 省下标
    private Integer province;
    // 市下标
    private Integer city;
    // 区下标
    private Integer district;
    // 昵称
    private String nick;
    // 生日
    private String birth;
    // 用户高清头像
    private String hdLogo;
    // 用户个人头像
    private String logo;
    // 手机号码
    private String phone;
    // 年龄
    private Integer age;
    // 星座
    private Integer constell;
    // 学校
    private String school;
    // 公司
    private String company;
    // 兴趣
    private String hobby;
    // job
    private Integer job;
    // 性别
    private Integer sex;
    // 创建时间
    private Date createAt;
    // 最后更新的时间
    private Date updateAt;
    // 通信ID
    private String imid;
    // 是否可见 1为可见可被搜索 0 为不可见
    private Integer visible;
    // 高清图片的宽度
    private Integer width;
    // 高清图片的高度
    private Integer height;
    
    //A是否点过B赞
    private boolean liked;
    //点赞数
    private int like;
    //咕吱签名
    public String intro;
    
    private Integer state;

    public static final Integer VISIBLE_YES = 1, VISIBLE_NO = 0;
    
    
    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getImid() {
        return imid;
    }

    public void setImid(String imid) {
        this.imid = imid;
    }

    public Integer getVisible() {
        return visible;
    }

    public void setVisible(Integer visible) {
        this.visible = visible;
    }

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGzno() {
        return gzno;
    }

    public void setGzno(String gzno) {
        this.gzno = gzno;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Integer getProvince() {
        return province;
    }

    public void setProvince(Integer province) {
        this.province = province;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    
	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public String getHdLogo() {
        return hdLogo;
    }

    public void setHdLogo(String hdLogo) {
        this.hdLogo = hdLogo;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getConstell() {
        return constell;
    }

    public void setConstell(Integer constell) {
        this.constell = constell;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public Integer getJob() {
        return job;
    }

    public void setJob(Integer job) {
        this.job = job;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public Integer getDistrict() {
        return district;
    }

    public void setDistrict(Integer district) {
        this.district = district;
    }

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}
	
    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int i) {
        this.like = i;
    }
    
    
    public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", gzno=" + gzno + ", pwd=" + pwd
				+ ", province=" + province + ", city=" + city + ", district="
				+ district + ", nick=" + nick + ", birth=" + birth
				+ ", hdLogo=" + hdLogo + ", logo=" + logo + ", phone=" + phone
				+ ", age=" + age + ", constell=" + constell + ", school="
				+ school + ", company=" + company + ", hobby=" + hobby
				+ ", job=" + job + ", sex=" + sex + ", createAt=" + createAt
				+ ", updateAt=" + updateAt + ", imid=" + imid + ", visible="
				+ visible + ", width=" + width + ", height=" + height
				+ ", liked=" + liked + ", like=" + like + ", state=" + state
				+ "]";
	}

	
    
}
