package com.guzhi.domain;

import java.util.Date;

/**
 * 用户点赞信息
 * 
 * @author duanshao
 * 
 */
public class PostsLike {
    private Long id;
    private Long postsId;
    private Long userId;
    private String gzno;
    private String userNick;
    private Date createAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPostsId() {
        return postsId;
    }

    public void setPostsId(Long postsId) {
        this.postsId = postsId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getGzno() {
        return gzno;
    }

    public void setGzno(String gzno) {
        this.gzno = gzno;
    }

    public String getUserNick() {
        return userNick;
    }

    public void setUserNick(String userNick) {
        this.userNick = userNick;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    @Override
    public String toString() {
        return "PostsLike [id=" + id + ", postsId=" + postsId + ", userId=" + userId + ", gzno=" + gzno + ", userNick="
                + userNick + ", createAt=" + createAt + "]";
    }
}
