package com.guzhi.domain;

import java.util.Date;

/**
 * @author duanshao
 * 
 */
public class DiscussionMember {
    private Long discussionId;
    private Long memberId;
    private Date createAt;

    /**
     * @return the discussionId
     */
    public Long getDiscussionId() {
        return discussionId;
    }

    /**
     * @param discussionId the discussionId to set
     */
    public void setDiscussionId(Long discussionId) {
        this.discussionId = discussionId;
    }

    /**
     * @return the memberId
     */
    public Long getMemberId() {
        return memberId;
    }

    /**
     * @param memberId the memberId to set
     */
    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    /**
     * @return the createAt
     */
    public Date getCreateAt() {
        return createAt;
    }

    /**
     * @param createAt the createAt to set
     */
    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

}
