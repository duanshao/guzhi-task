package com.guzhi.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 帖子评论表
 * 
 * @author xiejunbo
 * @email 624664181@qq.com
 * @version 1.0.0
 * @date 2014年12月5日 下午3:52:08
 **/
@JsonInclude(Include.NON_NULL)
public class Reply {

    private Long id;// 主键ID 自增

    private Long postsId; // 帖子id

    private Long postsUserId;// 楼主ID

    private Integer types;// 0 评论 1 回复 2 点赞

    public static final Integer REPLY_TYPE_COMMENT = 0, REPLY_TYPE_REPLY = 1, REPLY_TYPE_LIKE = 2;

    private Long userId;// 回复者ID/评论者ID/点赞者ID

    private String usernick;// 回复者/评论者/点赞者昵称

    private String userLogo;// 回复者/评论者/点赞者头像

    private Long replyId;// 回复某个评论的评论ID

    private Long replyUserId;// 回复的那条评论的作者id

    private String replyUsernick;// 回复的那条评论的作者昵称

    private String content;// 回复内容

    private String userIp;// 回复IP

    private Date updateAt;// 更新时间

    private Long masterReplyId;// 当出现回复回复的回复等情况时，保存对应的评论ID

    public Long getMasterReplyId() {
        return masterReplyId;
    }

    public void setMasterReplyId(Long masterReplyId) {
        this.masterReplyId = masterReplyId;
    }

    public void setReplyUsernick(String replyUsernick) {
        this.replyUsernick = replyUsernick;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the postsId
     */
    public Long getPostsId() {
        return postsId;
    }

    /**
     * @param postsId the postsId to set
     */
    public void setPostsId(Long postsId) {
        this.postsId = postsId;
    }

    /**
     * @return the postsUserId
     */
    public Long getPostsUserId() {
        return postsUserId;
    }

    /**
     * @param postsUserId the postsUserId to set
     */
    public void setPostsUserId(Long postsUserId) {
        this.postsUserId = postsUserId;
    }

    /**
     * @return the types
     */
    public Integer getTypes() {
        return types;
    }

    /**
     * @param types the types to set
     */
    public void setTypes(Integer types) {
        this.types = types;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the usernick
     */
    public String getUsernick() {
        return usernick;
    }

    /**
     * @param userNick the usernick to set
     */
    public void setUsernick(String usernick) {
        this.usernick = usernick;
    }

    /**
     * @return the userLogo
     */
    public String getUserLogo() {
        return userLogo;
    }

    /**
     * @param userLogo the userLogo to set
     */
    public void setUserLogo(String userLogo) {
        this.userLogo = userLogo;
    }

    /**
     * @return the replyId
     */
    public Long getReplyId() {
        return replyId;
    }

    /**
     * @param replyId the replyId to set
     */
    public void setReplyId(Long replyId) {
        this.replyId = replyId;
    }

    /**
     * @return the replyUserId
     */
    public Long getReplyUserId() {
        return replyUserId;
    }

    /**
     * @param replyUserId the replyUserId to set
     */
    public void setReplyUserId(Long replyUserId) {
        this.replyUserId = replyUserId;
    }

    /**
     * @return the replyUsernick
     */
    public String getReplyUsernick() {
        return replyUsernick;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the userIp
     */
    public String getUserIp() {
        return userIp;
    }

    /**
     * @param userIp the userIp to set
     */
    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    /**
     * @return the updateAt
     */
    public Date getUpdateAt() {
        return updateAt;
    }

    /**
     * @param updateAt the updateAt to set
     */
    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    @Override
    public String toString() {
        return "Reply [id=" + id + ", postsId=" + postsId + ", postsUserId=" + postsUserId + ", types=" + types
                + ", userId=" + userId + ", userNick=" + usernick + ", userLogo=" + userLogo + ", replyId=" + replyId
                + ", replyUserId=" + replyUserId + ", replyUserNick=" + replyUsernick + ", content=" + content
                + ", userIp=" + userIp + ", updateAt=" + updateAt + "]";
    }

}
