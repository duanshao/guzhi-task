package com.guzhi.domain.hx;

import com.guzhi.common.HXConstants;

/**
 * 环信接口URL组装类
 * @author guzhi
 *
 */

public class EndPoints {

	private final static String ROOT_TARGET = HXConstants.API_HTTP_SCHEMA + "://" 
	+ HXConstants.API_SERVER_HOST;

	/**
	 * 组装注册环信用户的URL
	 * 
	 * @return
	 */
	public static String getHXregisterUserUrl(){
		String replaceAppkey = HXConstants.APPKEY;
		String appkey = replaceAppkey.replace("#","/");
		String url = ROOT_TARGET + "/" + appkey + "/users";	
		return url;
	}
	
	/**
	 * 组装创建讨论组的URL
	 * 
	 * */
	public static String getChatGroupUrl(){
		String replaceAppkey = HXConstants.APPKEY;
		String appkey = replaceAppkey.replace("#","/");
		String url = ROOT_TARGET + "/" + appkey + "/chatgroups";
		return url;
	}
	
	
	/**
     * 组装获取token的URL
     * 
     * @return
     */
    public static String getTokenUrl(){
        String replaceAppkey = HXConstants.APPKEY;
        String appkey = replaceAppkey.replace("#","/");
        String url = ROOT_TARGET + "/" + appkey + "/token";
        return url;
    }
	
	/**
	 * 组装发送消息的URL
	 * 
	 * Path : /{org_name}/{app_name}/messages
	 * @return
	 */
	public static String getHXSendMsgUrl(){
		String replaceAppkey = HXConstants.APPKEY;
		String appkey = replaceAppkey.replace("#","/");
		String url = ROOT_TARGET + "/" + appkey + "/messages";
		return url;
	}
	
    /**
     * 组装更新环信密码的URL
     * 
     * Path : /{org_name}/{app_name}/users/{username}/password
     * @return 
     */
    public static String getHXupdatePasswordUrl(String username) {
        String replaceAppkey = HXConstants.APPKEY;
        String appkey = replaceAppkey.replace("#", "/");
        String url = ROOT_TARGET + "/" + appkey + "/users/" + username +"/password"; 
        return url;
    }

    /**
     * 组装查看用户在线状态的URL
     * Path : /{org_name}/{app_name}/users/{username}/status
     * @param username
     * @return
     */
    public static String getOnlineStatusUrl(String username) {
        String replaceAppkey = HXConstants.APPKEY;
        String appkey = replaceAppkey.replace("#", "/");
        String url = ROOT_TARGET + "/" + appkey + "/users/" + username + "/status";
        return url;
    }
	
}
