package com.guzhi.domain.hx.response;

public class ChatGroupRespData {

	private Long groupid;

	public Long getGroupid() {
		return groupid;
	}

	public void setGroupid(Long groupid) {
		this.groupid = groupid;
	}
	
}
