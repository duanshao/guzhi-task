package com.guzhi.domain.hx.response;

import java.util.Arrays;

public class MessageResp {

	private String action;
	
	private String application;
	
	private String uri;
	
	private String[] entities;
	
	private MessageDataResp data;
	
	private String timestamp;
	
	private String duration;
	
	private String organization;
	
	private String applicationName;

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String[] getEntities() {
		return entities;
	}

	public void setEntities(String[] entities) {
		this.entities = entities;
	}

	public MessageDataResp getData() {
		return data;
	}

	public void setData(MessageDataResp data) {
		this.data = data;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	@Override
	public String toString() {
		return "MessageResp [action=" + action + ", application=" + application
				+ ", uri=" + uri + ", entities=" + Arrays.toString(entities)
				+ ", data=" + data + ", timestamp=" + timestamp + ", duration="
				+ duration + ", organization=" + organization
				+ ", applicationName=" + applicationName + "]";
	}
	

}
