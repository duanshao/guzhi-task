package com.guzhi.domain.hx.response;

/**
 * 环信Token Bean
 * author Elson
 * 2014年11月6日
 * email 372069412@qq.com
 */
public class TokenResp {

    private String access_token;
    
    private Long expires_in;

    private String application;

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public Long getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(Long expires_in) {
		this.expires_in = expires_in;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	@Override
	public String toString() {
		return "TokenResult [access_token=" + access_token + ", expires_in="
				+ expires_in + ", application=" + application + "]";
	}
    
}
