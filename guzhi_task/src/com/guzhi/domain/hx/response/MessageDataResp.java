package com.guzhi.domain.hx.response;

public class MessageDataResp {

    private String toUserImId;

    public String getToUserImId() {
        return toUserImId;
    }

    public void setToUserImId(String toUserImId) {
        this.toUserImId = toUserImId;
    }

    @Override
    public String toString() {
        return "MessageDataResp [toUserImId=" + toUserImId + "]";
    }

}
