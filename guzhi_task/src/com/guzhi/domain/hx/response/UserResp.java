package com.guzhi.domain.hx.response;

import java.util.Arrays;

import com.guzhi.domain.hx.request.Entities;


public class UserResp {

	private String action;
	
	private String application;
	
	//private String params;
	
	private String path;
	
	private String uri;
	
	private Entities[] entities;
	
	private Long timestamp; 
	
	private int duration;
	
	private String organization;
	
	private String applicationName;

	@Override
	public String toString() {
		return "User [action=" + action + ", application=" + application
				+ ", path=" + path + ", uri=" + uri + ", entities="
				+ Arrays.toString(entities) + ", timestamp=" + timestamp
				+ ", duration=" + duration + ", organization=" + organization
				+ ", applicationName=" + applicationName + "]";
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Entities[] getEntities() {
		return entities;
	}

	public void setEntities(Entities[] entities) {
		this.entities = entities;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

}
