package com.guzhi.domain.hx.request;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChatgroupsReq {

	//群名称
	private String groupname;
	//群描述
	private String desc; 
	//群是否公开
	@JsonProperty("public")
	private boolean pub;
	//群组成员最大数(包括群主), 值为数值类型,默认值200,此属性为可选的
	private Integer maxusers;
	//加入公开群是否需要批准, 没有这个属性的话默认是true, 此属性为可选的
	private boolean approval;
	//群组的管理员, 此属性为必须的
	private String owner;
	//群组成员,此属性为可选的,但是如果加了此项,数组元素至少一个
	private String[] members;
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public boolean isPub() {
		return pub;
	}
	public void setPub(boolean pub) {
		this.pub = pub;
	}
	public Integer getMaxusers() {
		return maxusers;
	}
	public void setMaxusers(Integer maxusers) {
		this.maxusers = maxusers;
	}
	public boolean getApproval() {
		return approval;
	}
	public void setApproval(boolean approval) {
		this.approval = approval;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String[] getMembers() {
		return members;
	}
	public void setMembers(String[] members) {
		this.members = members;
	}
	@Override
	public String toString() {
		return "ChatgroupsReq [groupname=" + groupname + ", desc=" + desc
				+ ", pub=" + pub + ", maxusers=" + maxusers + ", approval="
				+ approval + ", owner=" + owner + ", members="
				+ Arrays.toString(members) + "]";
	}
	
}
