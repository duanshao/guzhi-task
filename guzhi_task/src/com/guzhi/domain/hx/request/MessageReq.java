package com.guzhi.domain.hx.request;

import java.util.Map;

/**
 * author Elson
 * 2014年11月20日 下午4:02:45
 * email 372069412@qq.com
 */
public class MessageReq {
    // users 给用户发消息, chatgroups 给群发消息
    private String target_type;

    // 注意这里需要用数组,数组长度建议不大于20, 即使只有
    // 一个用户u1或者群组, 也要用数组形式 ['u1'], 给用户发
    // 送时数组元素是用户名,给群组发送时数组元素是groupid
    private String[] target;

    // 消息内容
    private MsgReq msg;

    // 表示这个消息是谁发出来的, 可以没有这个属性, 那么就会显示是admin, 如果有的话, 则会显示是这个用户发出的
    private String from;

    // 扩展属性, 由app自己定义.可以没有这个字段，但是如果有，值不能是“ext:null“这种形式，否则出错

    private Map<String, Object> ext;

    public String getTarget_type() {
        return target_type;
    }

    public void setTarget_type(String target_type) {
        this.target_type = target_type;
    }

    public String[] getTarget() {
        return target;
    }

    public void setTarget(String[] target) {
        this.target = target;
    }

    public MsgReq getMsg() {
        return msg;
    }

    public void setMsg(MsgReq msg) {
        this.msg = msg;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Map<String, Object> getExt() {
        return ext;
    }

    public void setExt(Map<String, Object> ext) {
        this.ext = ext;
    }

}
