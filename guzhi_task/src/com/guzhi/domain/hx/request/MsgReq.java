package com.guzhi.domain.hx.request;
/**
 * author Elson
 * 2014年11月20日 下午4:03:18
 * email 372069412@qq.com
 */
public class MsgReq {
	// 消息类型
	private String type;
	//action1
	private String action;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

}
