package com.guzhi.domain.hx.request;

import com.guzhi.domain.User;

/**
 * author Elson
 * 2014年11月20日 下午4:04:04
 * email 372069412@qq.com
 */
public class ExtReq {

    private Integer state;
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

}
