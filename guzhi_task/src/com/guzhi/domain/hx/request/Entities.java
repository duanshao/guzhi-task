package com.guzhi.domain.hx.request;

public class Entities {

	private String uuid;
	
	private String type;
	
	private Long created;
	
	private Long modified;
	
	private String username;
	
	private boolean activated;

	@Override
	public String toString() {
		return "Entities [uuid=" + uuid + ", type=" + type + ", created="
				+ created + ", modified=" + modified + ", username=" + username
				+ ", activated=" + activated + "]";
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getCreated() {
		return created;
	}

	public void setCreated(Long created) {
		this.created = created;
	}

	public Long getModified() {
		return modified;
	}

	public void setModified(Long modified) {
		this.modified = modified;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}
}
