package com.guzhi.domain;

/**
 * 
 * @author xiejunbo
 * @email 624664181@qq.com
 * @version 1.0.0
 * @date 2014年12月19日 下午2:45:33
 **/

public class Relation {
    
    private Long id;
    
    private int userId;//当前用户
    
    private int targetId;//目标用户
    
    private int status;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return the targetId
     */
    public int getTargetId() {
        return targetId;
    }

    /**
     * @param targetId the targetId to set
     */
    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Relation [id=" + id + ", userId=" + userId + ", targetId=" + targetId + ", status=" + status + "]";
    }
    
}
