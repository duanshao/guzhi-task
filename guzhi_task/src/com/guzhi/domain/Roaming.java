package com.guzhi.domain;

public class Roaming {
	//用户开始时间
	private Long startTime;
	//判断用户是否漫游结束
	private boolean maxTime;
	//城市code
	private String cityCode;
	//失效时间
	private Long expire;
	//漫游时间周期
	private Integer period;

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public boolean getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(boolean maxTime) {
		this.maxTime = maxTime;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public Long getExpire() {
		return expire;
	}

	public void setExpire(Long expire) {
		this.expire = expire;
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}
}
