package com.guzhi.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Posts {
    @JsonProperty("postsId")
    private Long id;// 主键
    private Long userId;// 发帖人id
    private String nick;// 发帖人的昵称
    private String logo;// 发帖人的头像
    private Integer category;// 主题分类表外键
    private String cateName;// 主题分类名称
    private String title;// 帖子标题
    @JsonIgnore
    private String titlePinyin;// 标题拼音，为了方便全文检索
    private String content;// 帖子内容
    private Integer likes;// 点赞用户数
    private Integer replys;// 帖子回复数
    private String userIp;// 发帖人IP
    private Double lat;// 纬度
    private Double lon;// 经度
    private String location;// 具体位置的中文信息
    private Integer province;// 省份
    private Integer city;// 市区
    private Date createAt;// 发帖时间
    private Date updateAt;// 更新时间

    private List<PostsAlbum> albums;// 对应的相册

    private String titleMatch;

    /**
     * 真实的距离
     */
    private Double dist;

    private int liked; // liked = 1表示已点赞

    public Double getDist() {
        return dist;
    }

    public void setDist(Double dist) {
        if (dist == null) {
            return;
        }
        BigDecimal bd = new BigDecimal(dist);
        this.dist = bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public String getTitleMatch() {
        return titleMatch;
    }

    public void setTitleMatch(String titleMatch) {
        this.titleMatch = titleMatch;
    }

    public List<PostsAlbum> getAlbums() {
        return albums;
    }

    public void setAlbums(List<PostsAlbum> albums) {
        this.albums = albums;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the nick
     */
    public String getNick() {
        return nick;
    }

    /**
     * @param nick the nick to set
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the category
     */
    public Integer getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(Integer category) {
        this.category = category;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the titlePinyin
     */
    public String getTitlePinyin() {
        return titlePinyin;
    }

    /**
     * @param titlePinyin the titlePinyin to set
     */
    public void setTitlePinyin(String titlePinyin) {
        this.titlePinyin = titlePinyin;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return the likes
     */
    public Integer getLikes() {
        return likes;
    }

    /**
     * @param likes the likes to set
     */
    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    /**
     * @return the replys
     */
    public Integer getReplys() {
        return replys;
    }

    /**
     * @param replys the replys to set
     */
    public void setReplys(Integer replys) {
        this.replys = replys;
    }

    /**
     * @return the userIp
     */
    public String getUserIp() {
        return userIp;
    }

    /**
     * @param userIp the userIp to set
     */
    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }

    /**
     * @return the lat
     */
    public Double getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(Double lat) {
        this.lat = lat;
    }

    /**
     * @return the lon
     */
    public Double getLon() {
        return lon;
    }

    /**
     * @param lon the lon to set
     */
    public void setLon(Double lon) {
        this.lon = lon;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the province
     */
    public Integer getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(Integer province) {
        this.province = province;
    }

    /**
     * @return the city
     */
    public Integer getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(Integer city) {
        this.city = city;
    }

    /**
     * @return the createAt
     */
    public Date getCreateAt() {
        return createAt;
    }

    /**
     * @param createAt the createAt to set
     */
    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    /**
     * @return the updateAt
     */
    public Date getUpdateAt() {
        return updateAt;
    }

    /**
     * @param updateAt the updateAt to set
     */
    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public int getLiked() {
        return liked;
    }

    public void setLiked(int liked) {
        this.liked = liked;
    }

    @Override
    public String toString() {
        return "Posts [id=" + id + ", userId=" + userId + ", nick=" + nick + ", logo=" + logo + ", category="
                + category + ", title=" + title + ", titlePinyin=" + titlePinyin + ", content=" + content + ", likes="
                + likes + ", replys=" + replys + ", userIp=" + userIp + ", lat=" + lat + ", lon=" + lon + ", location="
                + location + ", province=" + province + ", city=" + city + ", createAt=" + createAt + ", updateAt="
                + updateAt + "]";
    }

}
