package com.guzhi.domain;

import java.util.Date;

/**
 * 互动表
 * 
 * @author duanshao
 * 
 */
public class Interact {
    private Long id;
    private Long fromUserId;
    private Long toUserId;
    private Integer type;
    public static final int TYPE_LIKE = 0, TYPE_HI = 1;
    private String content;
    private Date createAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    @Override
    public String toString() {
        return "Interact [id=" + id + ", fromUserId=" + fromUserId + ", toUserId=" + toUserId + ", type=" + type
                + ", content=" + content + ", createAt=" + createAt + "]";
    }

}
