package com.guzhi.domain;

public class UserTraceQuery {
	
    private Double lat;
    private Double lon;
    private Double dist;
    private Double latLeft;
    private Double latRight;
    private Double lonLeft;
    private Double lonRight;
    private Integer city;
    private Integer province;
    private Integer offset;
    private Integer size;
    private Integer sex;
    private Integer page;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getDist() {
        return dist;
    }

    public void setDist(Double dist) {
        this.dist = dist;
    }

    public Double getLatLeft() {
        return latLeft;
    }

    public void setLatLeft(Double latLeft) {
        this.latLeft = latLeft;
    }

    public Double getLatRight() {
        return latRight;
    }

    public void setLatRight(Double latRight) {
        this.latRight = latRight;
    }

    public Double getLonLeft() {
        return lonLeft;
    }

    public void setLonLeft(Double lonLeft) {
        this.lonLeft = lonLeft;
    }

    public Double getLonRight() {
        return lonRight;
    }

    public void setLonRight(Double lonRight) {
        this.lonRight = lonRight;
    }

    public Integer getCity() {
        return city;
    }

    public void setCity(Integer city) {
        this.city = city;
    }

    public Integer getProvince() {
		return province;
	}

	public void setProvince(Integer province) {
		this.province = province;
	}

	public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

}
