package com.guzhi.domain;

import java.util.Date;

/**
 * @author duanshao
 * 
 */
public class Discussion {
    private Long id;
    private Long leader;
    private String logo;
    private String name;
    private Date createAt;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the leader
     */
    public Long getLeader() {
        return leader;
    }

    /**
     * @param leader the leader to set
     */
    public void setLeader(Long leader) {
        this.leader = leader;
    }

    /**
     * @return the logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * @param logo the logo to set
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the createAt
     */
    public Date getCreateAt() {
        return createAt;
    }

    /**
     * @param createAt the createAt to set
     */
    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

}
