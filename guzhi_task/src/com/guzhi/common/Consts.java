package com.guzhi.common;

import java.util.HashMap;
import java.util.Map;

/**
 * 项目常量类
 * 
 * @author duanshao
 * 
 */
public interface Consts {

    String UTF8 = "utf-8";
    String PRODUCES = "application/json;charset=utf-8";

    // 漫游时间
    Map<String, Integer> POSTS_INDEX = new HashMap<String, Integer>() {
        private static final long serialVersionUID = 190232797062935098L;

        {
            put("110000", 0);// 北京市
            put("120000", 1);// 天津市
            put("130000", 2);// 河北省
            put("140000", 3);// 山西省
            put("150000", 4);// 内蒙古自治区
            put("210000", 5);// 辽宁省
            put("220000", 6);// 吉林省
            put("230000", 7);// 黑龙江省
            put("310000", 8);// 上海市
            put("320000", 9);// 江苏省
            put("330000", 10);// 浙江省
            put("340000", 11);// 安徽省
            put("350000", 12);// 福建省
            put("360000", 13);// 江西省
            put("370000", 14);// 山东省
            put("410000", 15);// 河南省
            put("420000", 16);// 湖北省
            put("430000", 17);// 湖南省
            put("440000", 18);// 广东省
            put("450000", 19);// 广西壮族自治区
            put("460000", 20);// 海南省
            put("500000", 21);// 重庆市
            put("510000", 22);// 四川省
            put("520000", 23);// 贵州省
            put("530000", 24);// 云南省
            put("540000", 25);// 西藏自治区
            put("610000", 26);// 陕西省
            put("620000", 27);// 甘肃省
            put("630000", 28);// 青海省
            put("640000", 29);// 宁夏回族自治区
            put("650000", 30);// 新疆维吾尔自治区
            put("710000", 31);// 台湾省
            put("810000", 32);// 香港特别行政区
            put("820000", 33);// 澳门特别行政区
        }

    };

    // redis前缀
    interface Cache {
        String POSTS_CATEGORY = "posts_category_";
        String USER_PREFIX = "user_";
        String ALBUM_PREFIX = "album_";
        String INTRO_PREFIX = "intro_";
        String USER_LOCATE_PREFIX = "user_locate_province_";
        String USER_CITY_PREFIX = "user_city";
        String USER_PROVINCE_PREFIX = "user_province";
    }

    interface Key {
        String SIGN = "123456";
    }

    // 表前缀
    interface TablePrefix {
        String USER = "user_";
        String ALBUM = "album_";
        String FRIENDSHIP = "friendship_";
        String INTRO = "intro_";
        String DISCUSSION_MEMBER = "discussion_member_";
        String POSTS = "posts_";
        String REPLY = "reply_";
        String POSTS_ALBUM = "posts_album_";
    }

    interface Roaming {
        Integer OUTTIME = 86400;
        Integer TIME = 3600000;
        Integer SETTIME = 3600;
    }

    // redis前缀
    interface RedisPrefix {
        String ROAM = "roam_";
        String ROAM_TIME = "roam_time_";
        String CITY = "city_";
    }

    interface Action {
        // 打招呼
        String HI = "hi";
        // 帖子互动
        String POSTS_REPLY = "posts_reply";
    }

    // 通用状态码
    interface Code {

        /** 公用的返回码 */
        // 成功
        int SUCCESS = 0;
        // 失败
        int FAIL = -1;
        // 签名错误
        int SIGN_ERROR = 1;
        // 网络链接错误
        int NET_CONN_ERROR = 2;

        // 安全错误
        int SECURE_ERROR = 3;
        // 内部错误,发生异常等
        int INTERNAL_ERROR = 4;
        // 参数错误
        int DATA_ERROR = 5;
        // 请求错误，参数不足
        int REQ_ERROR = 6;
        // 业务信息错误
        int APP_INFO_ERROR = 7;
        // 数据不存在
        int DATA_NO_FOUND = 8;

        // 网络响应错误
        int NET_READ_ERROR = 9;

        // 已点过赞
        int LIKED = 10;

        // 验证码超时
        int CODE_TIMEOUT = 11;

        /** 注册接口方法特定的返回码 1000 */
        // 用户注册.无效密码
        int USER_REGISTE_PWD_INVALID = 1001;
        // 用户注册.无效手机
        int USER_REGISTE_PHONE_INVALID = 1002;
        // 用户注册.无效验证码
        int VERIFY_CODE_INVALID = 1003;
        // 用户注册.无效昵称
        int USER_REGISTE_NICK_INVALID = 1004;
        // 用户注册.无效生日
        int USER_REGISTE_BIRTH_INVALID = 1005;
        // 用户注册.无效省下标
        int USER_REGISTE_PROVINCE_INVALID = 1006;
        // 用户注册.无效市下标
        int USER_REGISTE_CITY_INVALID = 1007;
        // 用户注册.无效县区下标
        int USER_REGISTE_DISTRICE_INVALID = 1008;
        // 用户注册.无效性别
        int USER_REGISTE_SEX_INVALID = 1009;
        // 用户注册.手机号码已经存在
        int USER_REGISTE_PHONE_EXISTS = 1010;
        // 搜索用户.是自己本身
        int USER_SEARCH_EXIST = 1011;

        /** 登录接口方法特定的返回码 2000 */
        // 用户登录.帐号或密码不正确
        int USER_LOGIN_FAIL_INVALID = 2001;
        // 用户登录.无效帐号
        int USER_LOGIN_USERNAME_INVALID = 2002;
        // 用户登录.无效密码
        int USER_LOGIN_PASSWORD_INVALID = 2003;

        /** 修改用户资料方法特定的返回码 3000 */
        // 修改用户.无效年龄
        int USER_SET_AGE_INVALID = 3001;
        // 修改用户.无效生日
        int USER_SET_BIRTH_INVALID = 3002;
        // 修改用户.无效市区下标
        int USER_SET_CITY_INVALID = 3003;
        // 修改用户.无效公司格式
        int USER_SET_COMPANY_INVALID = 3004;
        // 修改用户.无效星座下标
        int USER_SET_CONSTELL_INVALID = 3005;
        // 修改用户.无效县区下标
        int USER_SET_DISTRICT_INVALID = 3006;
        // 修改用户.无效学校
        int USER_SET_SCHOOL_INVALID = 3007;
        // 修改用户.无效兴趣
        int USER_SET_HOBBY_INVALID = 3008;

        // 用户不存在
        int USER_NOT_EXISTS = 4001;
        // 密码与原密码相同
        int SAME_PASSWORD = 4002;
        // 搜索自己
        int SEARCH_YOURSELF = 4003;

        // 漫游超时
        int ROAMING_OUTTIME = 5001;
        // 正在漫游
        int ROAMING_GOING = 5002;
        // 没有此用户
        int ROAMING_NULL = 5003;

        // 已经点过赞
        int POSTS_LIKED = 6001;
        // 帖子不存在
        int POSTS_NOT_EXIST = 6002;
        // 帖子不存在
        int REPLY_NOT_EXIST = 6003;
        int HXSERVICE_SET_PASSWORD_FAIL = 9001;

    }

    // 表分区的数量
    interface TablePartition {
        int USER = 8;
        int ALBUM = 8;
        int FRIENDSHIP = 8;
        int INTRO = 8;
        int DISCUSSION_MEMBER = 8;
        int REPLY = 4;
        int POSTS = 34;
        int POSTS_ALBUM = 34;
    }

    // 环信请求方法
    interface HTTPMethod {
        String METHOD_GET = "GET";
        String METHOD_POST = "POST";
        String METHOD_PUT = "PUT";
        String METHOD_DELETE = "DELETE";
    }

    // 环信的常量
    interface HXCONSTS {
        String TOKEN = "hx_token";
    }

    interface Roles {
        /** USER_ROLE_ORGADMIN value: orgAdmin */
        public static String USER_ROLE_ORGADMIN = "orgAdmin";

        /** USER_ROLE_APPADMIN value: appAdmin */
        public static String USER_ROLE_APPADMIN = "appAdmin";

        /** USER_ROLE_IMUSER value: imUser */
        public static String USER_ROLE_IMUSER = "imUser";
    }

    /** Cookie */
    interface Cookies {
        /** 定义cookie名字 */
        static final String COOKIE_ADMIN = "cookie_admin";
        /** 定义验证码保存在session的字符串 */
        static final String VERIFY_CODE = "verify_code";
        /** 定义cookie存活时间为: 一周 */
        static final int COOKIE_MAX_AGE = 7 * 24 * 60 * 60;
    }

    interface dist {
        double TRACE = 300;
        double POSTS = 300;
    }
}
