package com.guzhi.common;

/**
 * 环信常量类
 * author Elson
 * 2014年11月6日
 * email 372069412@qq.com
 */
public interface HXConstants {

    // API_HTTP_SCHEMA
    String API_HTTP_SCHEMA = PropertiesUtils.getProperties().getProperty("API_HTTP_SCHEMA");

    // AIP_SERVER_HOST SERVER
    String API_SERVER_HOST = PropertiesUtils.getProperties().getProperty("API_SERVER_HOST");

    // API_SERVER_PORT
    String API_SERVER_PORT = PropertiesUtils.getProperties().getProperty("API_SERVER_PORT");

    // APPKEY
    String APPKEY = PropertiesUtils.getProperties().getProperty("APPKEY");

    // APP_CLIENT_ID
    String APP_CLIENT_ID = PropertiesUtils.getProperties().getProperty("APP_CLIENT_ID");

    // APP_CLIENT_SECRET
    String APP_CLIENT_SECRET = PropertiesUtils.getProperties().getProperty("APP_CLIENT_SECRET");

    // APP_ADMIN_USERNAME
    String APP_ADMIN_USERNAME = PropertiesUtils.getProperties().getProperty("APP_ADMIN_USERNAME");

    // APP_ADMIN_PASSWORD
    String APP_ADMIN_PASSWORD = PropertiesUtils.getProperties().getProperty("APP_ADMIN_PASSWORD");

    // DEFAULT_PASSWORD
    String DEFAULT_PASSWORD = PropertiesUtils.getProperties().getProperty("DEFAULT_PASSWORD");

}
