package com.guzhi.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.springframework.stereotype.Service;

import com.guzhi.domain.city.CityPath;
import com.guzhi.utils.JsonHelper;

/**
 * 属性文件工具类
 * author Elson
 * 2014年11月6日
 * email 372069412@qq.com
 */
@Service
public class PropertiesUtils {

    public static Properties getProperties() {

        Properties p = new Properties();
        try {
            // 读取环信RestAPI配置文件RestAPIConfig.properties
            InputStream inputStream = PropertiesUtils.class.getClassLoader().getResourceAsStream(
                    "RestAPIConfig_test.properties");
            p.load(inputStream);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return p;
    }

    public static Properties getRegexpProperties() {
        Properties p = new Properties();
        try {
            // 读取正则表达式配置文件regexp.properties
            InputStream inputStream = PropertiesUtils.class.getClassLoader().getResourceAsStream("regexp.properties");
            p.load(inputStream);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return p;
    }

    public static void main(String[] args) {

        InputStream input = PropertiesUtils.class.getClassLoader().getResourceAsStream("city.txt");
        InputStreamReader read = null;
        BufferedReader buffered = null;
        Integer code = 130600;

        try {
            read = new InputStreamReader(input, "utf-8");
            buffered = new BufferedReader(read);
            String line = buffered.readLine();

            if (!"".equals(line) && null != line) {
                CityPath city = JsonHelper.fromJson(line, CityPath.class);
                for (int i = 0; i < city.getDist().length; i++) {
                    for (int j = 0; j < city.getDist()[i].getCity().length; j++) {

                        if (city.getDist()[i].getCity()[j].getId().equals(code)) {
                            System.out.println(city.getDist()[i].getCity()[j].getLat());
                            System.out.println(city.getDist()[i].getCity()[j].getLon());
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                input.close();
                buffered.close();
                read.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}