package com.guzhi.common;
/**
 * 注册接口常量工具类
 * author Elson
 * 2014年11月28日 下午4:54:39
 * email 372069412@qq.com
 */
public interface ConstsRegExpUtils {

	//验证手机号码
	String PHONE = PropertiesUtils.getRegexpProperties().getProperty("phone");
	//验证年龄
	String AGE = PropertiesUtils.getRegexpProperties().getProperty("age");
	//验证星座
	String CONSTELL = PropertiesUtils.getRegexpProperties().getProperty("constell");
	//验证地区
	String AREA = PropertiesUtils.getRegexpProperties().getProperty("area");
	//验证生日
	String BIRTH = PropertiesUtils.getRegexpProperties().getProperty("birth");
	//验证性别
	String SEX = PropertiesUtils.getRegexpProperties().getProperty("sex");
	//验证密码
	String PWD = PropertiesUtils.getRegexpProperties().getProperty("pwd");
	//验证登录账号
	String USERNAME = PropertiesUtils.getRegexpProperties().getProperty("username");
	//验证公司
	String COMPANY = PropertiesUtils.getRegexpProperties().getProperty("company");
	//验证兴趣
	String HOBBY = PropertiesUtils.getRegexpProperties().getProperty("hobby");
	//验证咕吱签名的内容
	String INTRO_CONTENT = PropertiesUtils.getRegexpProperties().getProperty("intro_content");
	
}
