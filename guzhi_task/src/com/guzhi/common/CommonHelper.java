package com.guzhi.common;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.guzhi.domain.Resp;

/**
 * 公共辅助类
 * @abthor Elson
 * Nov 26, 2014 2:28:09 AM
 * @Email 372069412@qq.com
 */
public class CommonHelper {

	/**
	 * 检查userId是否合法
	 * @param userId
	 * @return
	 */
	public static Resp checkUserId(Long userId){
		if( null == userId || userId <= 0 ){
			return new Resp(Consts.Code.DATA_ERROR,"ID不合法,");
		}
		return null;
	}
	
	/**
	 * 过滤Bean属性
	 * @param <T>
	 * */
	public static <T> T filterBean(T bean,Object[] params){

		try{
			if( null != bean ){
				Class clas = bean.getClass();
				if( params.length > 0 ){
					for( int i = 0; i < params.length; i++ ){
						String methodName = "set" + params[i];
						System.out.println(methodName);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return bean;
	}
	
	/**
	 * 中文字符串转换
	 * 
	 * @param str	转换的字符串
	 * @return int	返回长度
	 * */
	public static int transformToStr(String str){
		int len = 0;
		String chinese = "[\u4e00-\u9fa5]";
		for(int i=0;i<str.length();i++){
			String temp = str.substring(i,i+1);
			len++;
			if( temp.matches(chinese) ){ //判断是否为中文
				len++;
			}
		}
		return len;
	}
	
	/**
	 * 判断是否为字母
	 * @param c
	 * @return
	 */
	private static boolean isLetter(char c){
		int k = 0x80;
		return c/k == 0?true:false;
	}
	
	/**
	 * 根据出生日期获取年龄和星座
	 * @param date 出生日期
	 * @return map
	 */
	public static Map<String,Integer> getAgeAndConstell(String date){
		
		Map<String,Integer> result = new HashMap<>();
		
		String[] strs = date.split("-");
		
		//根据生日计算出年龄
        int year = Integer.valueOf(strs[0]);
        //获取当前年份
        Calendar calendar = Calendar.getInstance();
        int current = calendar.get(Calendar.YEAR);
       
        Integer age = (int)current - year;
        if( age <= 0 ){
        	return null;
        }
        result.put("age",age);
		
    	Integer month = Integer.valueOf(strs[1]);
    	Integer day = Integer.valueOf(strs[2]);
    	
    	int stamptime = month * 100 + day;
    	    	
    	if( stamptime >= 310 && stamptime <= 419 ){
    		result.put("constell",1);
    	}else if( stamptime >= 420 && stamptime <= 520 ){
    		result.put("constell",2);
    	}else if( stamptime >= 521 && stamptime <= 621 ){
    		result.put("constell",3);
    	}else if( stamptime >= 622 && stamptime <= 722 ){
    		result.put("constell",4);
    	}else if( stamptime >= 723 && stamptime <= 822 ){
    		result.put("constell",5);
    	}else if( stamptime >= 823 && stamptime <= 922 ){
    		result.put("constell",6);
    	}else if( stamptime >= 923 && stamptime <= 1023 ){
    		result.put("constell",7);
    	}else if( stamptime >= 1024 && stamptime <= 1122 ){
    		result.put("constell",8);
    	}else if( stamptime >= 1123 && stamptime <= 1221 ){
    		result.put("constell",9);
    	}else if( stamptime >= 1222 || stamptime <= 119 ){
    		result.put("constell",10);
    	}else if( stamptime >= 120 && stamptime <= 218 ){
    		result.put("constell",11);
    	}else if( stamptime >= 219 && stamptime <= 320 ){
    		result.put("constell",12);
    	}  	
		return result;
	}

	
	
}
