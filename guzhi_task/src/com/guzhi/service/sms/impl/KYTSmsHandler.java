package com.guzhi.service.sms.impl;

import org.springframework.stereotype.Service;

import com.guzhi.service.sms.SmsHandler;

/**
 * 快易通短信接口
 * 
 * @author duanshao
 * 
 */
@Service("kytSmsHandler")
public class KYTSmsHandler implements SmsHandler {

    @Override
    public int send(String phone, String content) {
        return 0;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.guzhi.service.sms.SmsHandler#isAlive()
     */
    @Override
    public boolean isAlive() {
        // TODO Auto-generated method stub
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.guzhi.service.sms.SmsHandler#setAlive(boolean)
     */
    @Override
    public void setAlive(boolean isAvalie) {
        // TODO Auto-generated method stub

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.guzhi.service.sms.SmsHandler#addFails()
     */
    @Override
    public void addFails() {
        // TODO Auto-generated method stub

    }

}
