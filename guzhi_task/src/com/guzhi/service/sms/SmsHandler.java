package com.guzhi.service.sms;

public interface SmsHandler {
    int CODE_SUCCESS = 0;// 发送成功
    int CODE_NO_BALANCE = 1;// 余额不足
    int CODE_FAIL = -1;// 发送失败，包括网络异常

    int FAIL_THRESHOLD = 5;// 失败次数的阀值，连续失败了5次则需要更新handler为不可用 Alive为false

    /**
     * 发送短信
     * 
     * @param phone
     * @param content
     * @return
     */
    int send(String phone, String content);

    /**
     * handler目前是否可用
     * 
     * @return
     */
    boolean isAlive();

    /**
     * 设置handler目前是否可用
     * 
     * @return
     */
    void setAlive(boolean isAvalie);

    /**
     * 增加一次接口失败次数，当失败次数达到一定的限制应该设置handler为不可用，此方法需要做多线程同步
     */
    void addFails();

}
