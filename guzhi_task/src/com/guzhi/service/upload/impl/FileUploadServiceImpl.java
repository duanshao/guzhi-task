package com.guzhi.service.upload.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import org.csource.fastdfs.StorageClient1;
import org.csource.fastdfs.StorageServer;
import org.csource.fastdfs.TrackerServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.guzhi.service.upload.FileUploadService;
import com.guzhi.service.upload.pool.ConnectionPool;

@Service
public class FileUploadServiceImpl implements FileUploadService {

    private int waitTimes = 3;

    @Autowired
    private ConnectionPool pool;

    @Override
    public String uploadFile(File file) throws IOException, Exception {
        return uploadFile(file, getFileExtName(file.getName()));
    }

    @Override
    public String uploadFile(File file, String suffix) throws IOException, Exception {
        byte[] fileBuff = getFileBuffer(file);
        return send(fileBuff, suffix);
    }

    @Override
    public String uploadFile(byte[] fileBuff, String suffix) throws IOException, Exception {
        return send(fileBuff, suffix);
    }

    private String send(byte[] fileBuff, String fileExtName) throws IOException, Exception {
        String upPath = null;
        TrackerServer trackerServer = null;
        try {
            trackerServer = pool.checkout(waitTimes);

            StorageServer storageServer = null;
            StorageClient1 client1 = new StorageClient1(trackerServer, storageServer);
            long start = System.currentTimeMillis();
            upPath = client1.upload_file1(fileBuff, fileExtName, null);
            System.out.println("upload......................" + fileBuff.length + "==cost:"
                    + (System.currentTimeMillis() - start));
            pool.checkin(trackerServer);
        } catch (InterruptedException e) {
            // 确实没有空闲连接,并不需要删除与fastdfs连接
            throw e;
        } catch (NullPointerException e) {
            throw e;
        } catch (Exception e) {
            // 发生io异常等其它异常，默认删除这次连接重新申请
            e.printStackTrace();
            pool.drop(trackerServer);
            throw e;
        }
        return upPath;
    }

    private String getFileExtName(String name) {
        String extName = null;
        if (name != null && name.contains(".")) {
            extName = name.substring(name.lastIndexOf(".") + 1);
        }
        return extName;
    }

    private byte[] getFileBuffer(File file) {
        byte[] fileByte = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            fileByte = new byte[fis.available()];
            fis.read(fileByte);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                }
            }
        }
        return fileByte;
    }

    private byte[] getBuffer(InputStream is) {
        byte[] fileByte = null;
        try {
            fileByte = new byte[is.available()];
            is.read(fileByte);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileByte;
    }

    public int getWaitTimes() {
        return waitTimes;
    }

    public void setWaitTimes(int waitTimes) {
        this.waitTimes = waitTimes;
    }

    @Override
    public boolean deleteFile(String fileId) throws IOException, Exception {
        boolean result = false;
        TrackerServer trackerServer = null;
        try {
            trackerServer = pool.checkout(waitTimes);
            StorageServer storageServer = null;
            StorageClient1 client1 = new StorageClient1(trackerServer, storageServer);
            result = client1.delete_file1(fileId) == 0 ? true : false;
            pool.checkin(trackerServer);
        } catch (Exception e) {
            pool.drop(trackerServer);
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    @Override
    public byte[] getFileByID(String fileId) throws IOException, Exception {
        byte[] result = null;
        TrackerServer trackerServer = null;
        try {
            trackerServer = pool.checkout(waitTimes);
            StorageServer storageServer = null;
            StorageClient1 client1 = new StorageClient1(trackerServer, storageServer);
            result = client1.download_file1(fileId);
            pool.checkin(trackerServer);
        } catch (Exception e) {
            pool.drop(trackerServer);
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    @Override
    public String uploadFile(InputStream inputStream, String fileExtName) throws Exception {
        return send(getBuffer(inputStream), fileExtName);
    }

    public static void main(String[] args) {
        long nanos = TimeUnit.SECONDS.toNanos(2L);
        System.out.println(nanos);

    }
}
