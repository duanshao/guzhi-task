package com.guzhi.service.upload;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * 图片文件上传
 * 
 */
public interface FileUploadService {
    /**
     * 按文件流上传文件
     * 
     * @param inputStream
     * @param fileExtName
     * @return
     * @throws Exception
     */

    String uploadFile(InputStream inputStream, String fileExtName) throws Exception;

    /**
     * 上传文件
     * 
     * @param file 文件
     *            文件扩展名通过file.getName()获得
     * @return 文件存储路径
     * @throws IOException
     * @throws Exception
     */
    String uploadFile(File file) throws IOException, Exception;

    /**
     * 上传文件
     * 
     * @param file 文件
     * @param suffix 文件扩展名，如 (jpg,txt)
     * @return 文件存储路径
     * @throws IOException
     * @throws Exception
     */
    String uploadFile(File file, String suffix) throws IOException, Exception;

    /**
     * 上传文件
     * 
     * @param fileBuff 二进制数组
     * @param suffix 文件扩展名 ，如(jpg,txt)
     * @return
     */
    String uploadFile(byte[] fileBuff, String suffix) throws IOException, Exception;

    /**
     * 删除文件
     * 
     * @param fileId 包含group及文件目录和名称信息
     * @return true 删除成功，false删除失败
     * @throws IOException
     * @throws Exception
     */
    boolean deleteFile(String fileId) throws IOException, Exception;

    /**
     * 查找文件内容
     * 
     * @param fileId
     * @return 文件内容的二进制流
     * @throws IOException
     * @throws Exception
     */
    byte[] getFileByID(String fileId) throws IOException, Exception;
}
