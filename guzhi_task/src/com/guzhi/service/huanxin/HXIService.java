package com.guzhi.service.huanxin;

import java.util.Map;

import com.guzhi.domain.hx.request.ChatgroupsReq;
import com.guzhi.domain.hx.request.MessageReq;
import com.guzhi.domain.hx.request.TokenReq;
import com.guzhi.domain.hx.response.ChatGroupResp;
import com.guzhi.domain.hx.response.TokenResp;

/**
 * author Elson
 * 2014年11月6日
 * email 372069412@qq.com
 */
public interface HXIService {

    /**
     * 环信注册用户
     * 
     * @param userName 帐号
     * @param password 密码
     * @return 返回用户的UUID
     * */
    public String register(String userName, String password,String nick);

    /**
     * 环信发送透传消息
     * 
     * @param msg
     * */
    public String sendMsg(MessageReq msg);

    /**
     * 获取环信token值
     * 
     * @param token-Bean
     * @return TokenResult-Bean
     * */
    public TokenResp getToken(TokenReq token);

    /**
     * 更新环信用户密码
     * 
     * @param phone 手机号
     * @param newPassword
     * @param imid
     * @return
     */
    public boolean updateHXPassword(String newPassword, String phone);

    /**
     * 创建环信群
     * 
     * @param ChatgroupsReq
     * @return ChatGroupResp;
     * 
     * */
    public ChatGroupResp createChatGroup(ChatgroupsReq chatGroup);

    /**
     * 查看用户在线状态
     * 
     * @return
     */
    public boolean onlineStatus(String username);

    /**
     * 统计在线用户数
     * 
     * @return
     */
    public Long countOnlineUsers();

    /**
     * 发送环信透传信息
     * 
     * @param from
     * @param to
     * @param action
     * @param ext
     * @return
     */
    boolean sendMsg(String from, String[] to, String action, Map<String, Object> ext);

}
