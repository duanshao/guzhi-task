/**
 * 
 */
package com.guzhi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.guzhi.common.Consts;
import com.guzhi.domain.DomainResource;
import com.guzhi.domain.PostsCategory;

/**
 * @author duanshao
 * 
 */
@Service
public class PostsService {
    @Autowired
    private DomainResource resource;

    @Autowired
    private RedisService redis;

    public PostsCategory getCategory(Long id) {
        String key = Consts.Cache.POSTS_CATEGORY + id;
        PostsCategory category = redis.get(key, PostsCategory.class);
        if (category == null) {
            category = resource.getPostsCategoryById(id);
            if (category != null) {
                redis.set(key, category);
            }
        }
        return category;
    }
}
